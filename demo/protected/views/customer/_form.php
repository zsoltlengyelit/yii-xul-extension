<?php
/* @var $this CustomerController */
/* @var $model Customer */
/* @var $form ZoolActiveForm */

echo Xul::beginGroupbox(null, 1, $model->isNewRecord ? 'New customer' : 'Edit customer');

echo Xul::beginHbox(array('flex'=>1));



$form = $this->beginWidget('ZoolActiveForm', array(
		'method'=>'POST',
		'autofocus'=>$model->isNewRecord ? 'nameField' :null,
		'action'=> $model->isNewRecord ? $this->createAbsoluteUrl('customer/create') : $this->createAbsoluteUrl('customer/update', array('id'=>$model->id)),
));

echo Xul::beginVbox(array('flex'=>1));

					echo Xul::vbox($form->textFieldRow($model, 'name', array('flex'=>1, 'size'=>100, 'id'=>$model->isNewRecord ? 'nameField' : Xul::uniqueId())));			
	
					echo Xul::vbox($form->textFieldRow($model, 'phone', array('id'=>Xul::uniqueId())));					
		
					echo Xul::vbox($form->textFieldRow($model, 'address', array('id'=>Xul::uniqueId())));
								
		
				echo Xul::beginHbox();
				
					if($model->isNewRecord){
						echo $form->submit(' Save',array(),
								array('flex'=>1,
										'accesskey'=>'S',
										'image'=> Yii::app()->request->getBaseUrl(true) . '/images/save.png',)
						);
					}else{
						echo $form->submit('Save',
								array(
										'reRender'=>'customers',
										'onbeforesubmit'=>'window.currentTableIndex = Zool.byId("customertable").currentIndex;',
										'oncomplete'=>'Zool.byId("detailsdeck").selectedIndex = Zool.byId("customertable").currentIndex = window.currentTableIndex; '
								),
								array('flex'=>6, 'accesskey'=>'S')
						);
					}
						
						if(!$model->isNewRecord){
							
							echo Xul::linkButton('Delete',
									$this->createAbsoluteUrl('customer/delete', array('id'=>$model->id)),
									array('confirm'=>true, 'confirmContent'=> 'Are you sure you want to delete?'),
									array('flex'=>1));
							
						}
				echo Xul::endHbox();
			
			echo Xul::endVbox();
			
			echo Xul::spacer(2);
		
		$this->endWidget();
	
	echo Xul::endHbox();

echo Xul::endGroupbox();

echo Xul::box('', array('flex'=>1));
