<?php
/* @var $this CustomerController */
/* @var $dataProvider CustomerDataProvider */
/* @var $tabbox ZoolTabbox */

$this->pageTitle=Yii::app()->name . '';


echo Xul::beginVbox(array('id'=>'customer-page','flex'=>1));


	
// -------------------- filter -------------------------------
echo Xul::beginHbox(array('flex'=>1));	
		
		// search form
		$searchForm = $this->beginWidget('ZoolActiveForm', array(
				'id'=>'customer-search',
				'submitConfig'=>
				array('reRender'=>'customer-page'),
		));
					
		echo $searchForm->textField($dataProvider->getFilter(), 'filter', array(
				'emptytext'=>'Search...',
				'flex'=>1, 'id'=>Xul::uniqueId()));
			
		$this->endWidget();
		
		echo Xul::spacer(1);
		
		echo Xul::linkButton('Refresh', $this->createAbsoluteUrl('customer/index'), array('reRender'=>'customer-page'));

echo Xul::endHbox();

// ----------------------- grid ---------------------------

echo Xul::beginHbox(array('flex'=>1));
	
	$this->widget('ZoolGridView', array(
			'id'=>'customers',
			'dataProvider'=>$dataProvider,
			'treeOptions'=>array('id'=>'customertable'),
			'htmlOptions'=>array('flex'=>2),
			'summaryText'=> '',
			'onselect' => "Zool.byId('detailsdeck').selectedIndex = this.currentIndex;",
			'pager'=>array(
					'reRender'=>'detailsdeck'
			),
			'columns'=>array('name', 'phone', 'address')
	));
	
	
	$this->renderPartial('_detailsPanel', array('dataProvider'=>$dataProvider));


echo Xul::endHbox();


echo Xul::endVbox();
