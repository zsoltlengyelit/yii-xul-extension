<?php
/* @var $this CustomerController */
/* @var $dataProvider CustomerDataProvider */
?>

<deck
    id="detailsdeck"
    flex="1"> <?php

    foreach ($dataProvider->getData() as $model){
		echo Xul::beginBox();
    	$this->renderPartial('_form', array('model'=>$model));  
		echo Xul::endBox();
	}
	?>

</deck>
