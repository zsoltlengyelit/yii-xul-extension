<?php
/* @var $this EstateController */
/* @var $tabbox ZoolTabbox */
/* @var $searchForm ZoolActiveForm */

$tabbox = $this->beginWidget('ZoolTabbox', array(
		'id'=>'estates-tabs',
		'ajaxRendered'=>true,
		'tabboxOptions'=>array('flex'=>1),
		));

		if($tabbox->beginTab('Own estates', 'own-estates', array(
				'tabpanelId'=>'own-estates-tabpanel',
				'ajaxRendered'=>false,
				))){
			
				
				
				echo Xul::beginVbox(array('flex'=>1));
				
					echo Xul::beginHbox(array('flex'=>1));
					
						// -------------------- filter -------------------------------
					
						echo Xul::box('Estates of user "' . Yii::app()->user->name . '"');
						
						echo '<spacer flex="1" />';
					
						$ownEstaes = new EstateDataProvider(true);
						// search form
						
						$searchForm = $this->beginWidget('ZoolActiveForm', array(
											'id'=>'own-estate-search',
											'submitConfig'=>
												array('reRender'=>'own-estates-tabpanel'),
											));
							
							echo Xul::command('submit-own-estate-search', $searchForm->getSubmitScript());
														
							echo $searchForm->dropDownList($ownEstaes->getFilter(), 'sold', 
									array(
											'true' => 'Sold',
											'false'=> 'Not sold',
											''=>'All'),
									array(
											'oncommand'=>$searchForm->getSubmitScript()));
							/*
							echo $searchForm->checkBoxRow($ownEstaes->getFilter(), 'sold', array(
									'command'=>'submit-own-estate-search'
									));
							*/
							
							echo $searchForm->textField($ownEstaes->getFilter(), 'filter', array(
									'emptytext'=>'Search...',
									'flex'=>1, 'id'=>Xul::uniqueId()));
							
							//echo $searchForm->submit('Search', array('reRener'=>'own-estates-tabpanel'));
							
						$this->endWidget();
				
					echo Xul::endHbox();
					
					// -------------------------------------------------------------------
					
				//  ------------------------------ grid ---------------------------
				
				$this->widget('ZoolGridView', array(
					'id'=>'own-estes-grid',
					'dataProvider'=> $ownEstaes,
					'treeOptions'=>array('id'=>'estatetable', 'enableColumnDrag'=>true, 'hidecolumncpicker'=>false),
					'htmlOptions'=>array('flex'=>2),
					'summaryText'=> '',
					'onselect' => "Zool.byId('estatedetailsdeck').selectedIndex = this.currentIndex;",
					'pager'=>array(
							'reRender'=>'estatedetailsdeck'
					),
					'columns'=>array('title', 'address', array(
							'type'=>'raw',
							'name'=>'customer_id',
							'value'=>'$data->customer->name',
							),
							array(
									'type'=>'raw',
									'name'=>'address',
									'value'=>'$data->address',
							),														
							'state', 
							array(
									'type'=>'raw',
									'name'=>'size',
									'value'=>'empty($data->size) ? \'\' : $data->size  . \' m²\' ',	
							),
							array(
									'type'=>'raw',
									'name'=>'sold',
									'value'=>'$data->sold ? "Yes" : "No" ',
							),
							array(
									'type'=>'raw',
									'name'=>'price',
									'value'=>'empty($data->price) ? \'\' : $data->price  . \' $\' ',
							),
							
						)
				));
				
				// deck
				echo Xul::beginBox(array('flex'=>1));
				
					$this->renderPartial('_editDesk', array('dataProvider'=>$ownEstaes, 'comfortDataProvider'=> $comfortDataProvider));
				
				echo Xul::endBox();
				
			echo Xul::endVbox();
			
			$tabbox->endTab();
		}
		
		// other estates
		if($tabbox->beginTab('Estates of others', 'other-estates', array(
				'tabpanelId'=>'other-estates-tabpanel',
				'ajaxRendered'=>true,
				))){

			echo Xul::beginVbox(array('flex'=>1));
			
				// filter---------------------------------------
				
			echo Xul::beginHbox(array('flex'=>1));
				
			// -------------------- filter -------------------------------
				
				echo Xul::box('Estates of other users.');
				
				echo '<spacer flex="1" />';
					
				$otherEstates = new EstateDataProvider(false, 25);
				// search form
				$searchForm = $this->beginWidget('ZoolActiveForm', array(
						'id'=>'other-estate-search',
						'submitConfig'=>
						array('reRender'=>'other-estates-tabpanel'),
				));
					
				echo Xul::command('submit-other-estate-search', $searchForm->getSubmitScript());
				
				echo $searchForm->dropDownList($otherEstates->getFilter(), 'sold',
						array(
								'true' => 'Sold',
								'false'=> 'Not sold',
								''=>'All'),
						array(
								'oncommand'=>$searchForm->getSubmitScript()));
			
				echo $searchForm->textField($otherEstates->getFilter(), 'filter', array(
						'emptytext'=>'Search...',
						'flex'=>1, 'id'=>Xul::uniqueId()));
					
			
					
				$this->endWidget();
				
				echo Xul::endHbox();
					
			
				// end of filter --------------------------------
				
				$this->widget('ZoolGridView', array(
						'id'=>'others-estes-grid',
						'dataProvider'=> $otherEstates,
						'treeOptions'=>array('id'=>'estatetable2'),
						'htmlOptions'=>array('flex'=>2),
						'summaryText'=> '',
						//'onselect' => "Zool.byId('userdetailsdeck').selectedIndex = this.currentIndex;",
						'pager'=>array(								
								//'reRender'=>'userdetailsdeck'
						),
						'columns'=>array('title', 'description', 'address', array(
								'type'=>'raw',
								'name'=>'customer_id',
								'value'=>'$data->customer->name',
						),array(
								'type'=>'raw',
								'name'=>'agent_id',
								'value'=>'$data->agent->fullname',
						),
						'state',
						array(
								'type'=>'raw',
								'name'=>'size',
								'value'=>'empty($data->size) ? \'\' : $data->size  . \' m²\' ',
						),
						array(
								'type'=>'raw',
								'name'=>'sold',
								'value'=>'$data->sold ? "Yes" : "No" ',
						),
						array(
								'type'=>'raw',
								'name'=>'price',
								'value'=>'empty($data->price) ? \'\' : $data->price  . \' $\' ',
						),
								
								)
				));
			echo Xul::endVbox();
				
			$tabbox->endTab();
		}


$this->endWidget();
