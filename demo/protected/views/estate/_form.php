<?php
/* @var $this EstateController */
/* @var $form ZoolActiveForm */
/* @var $estate Estate */

echo Xul::beginGroupbox(Xul::uniqueId(), null,
		 $estate->isNewRecord ? 'New estate' : 'Edit estate (ID: '. $estate->id .')', 
		array('align'=>'left'));
	
	$form = $this->beginWidget('ZoolActiveForm', array(
			'method'=>'POST',
			'xmlOptions'=>array('flex'=>1),
			'tagName'=>'vbox',
			'action'=> empty($estate->id) ? $this->createAbsoluteUrl('estate/create') : $this->createAbsoluteUrl('estate/update', array('id'=>$estate->id)),
	));
		
		echo Xul::beginHbox(array('align'=>'left'));
			
			// --------------------- textfields ---------------------
			echo Xul::beginGroupbox(null, null, 'Basic data');
			
			echo Xul::beginVbox(array('flex'=>1));
			
			echo Xul::vbox($form->textFieldRow($estate, 'title', array('flex'=>1, 'id'=>Xul::uniqueId())));
			
			echo Xul::vbox($form->textFieldRow($estate, 'description', array('id'=>Xul::uniqueId())));
			
			echo Xul::vbox($form->textFieldRow($estate, 'address', array('id'=>Xul::uniqueId())));
				
			echo Xul::vbox(
					Xul::label('Size', 'size').
			
					Xul::box(
							$form->numberField(
									$estate,
									'size',
									array('id'=>Xul::uniqueId(), 'size'=>3)
							). ' m²')
					 
			);
			 
			echo Xul::vbox(
					Xul::label('Price', 'price').
						
					Xul::box(
							$form->numberField(
									$estate,
									'price',
									array('id'=>Xul::uniqueId(), 'size'=>5)
							). ' $')
			
			);
			 
			
			echo Xul::endVbox();
			echo Xul::endGroupbox();
			
			
			// ------------ states, infos ------------------------
			 
			echo Xul::beginGroupbox(null, null, 'State');

				echo Xul::beginVbox(array('flex'=>1));
				 
					echo Xul::vbox(
							Xul::box('State:').
					
							$form->radiogroup(
									$estate,
									'state',
									Estate::statesMap(),
									array('id'=>Xul::uniqueId())
							)
					);
					
					echo Xul::vbox(
							Xul::box('Sold').
							$form->dropDownList(
									$estate,
									'sold',
									array('1'=>'Yes', '0'=>'No'),
									array('id'=>Xul::uniqueId()))
					);
				 
				 
				echo Xul::endVbox();
				 
			echo Xul::endGroupbox();
			
			// -------------- Comforts ----------------------------
			
			echo Xul::beginGroupbox(null, null, 'Comforts');
			echo Xul::beginVbox(array('flex'=>1));
				
			$this->renderPartial('_estateComforts',
					array('estate'=>$estate, 'comfortDataProvider'=>$comfortDataProvider, 'form'=>$form));
				
			
			echo Xul::endVbox();
			echo Xul::endGroupbox();
			 
			// -------------- Persons ----------------------------
			
			echo Xul::beginGroupbox(null, null, 'Related persons');
			
				echo Xul::beginVbox(array('flex'=>1));
				 
				echo Xul::vbox(
						Xul::box('Agent').
						 
						$form->dropDownList(
								$estate,
								'agent_id',
								Xul::listData(User::model()->agent()->findAll(array('order'=>'fullname')), 'id', 'fullname')
								,array('id'=>Xul::uniqueId()))
				);
				 
				
				echo Xul::vbox(
						Xul::box('Customer').
						 
						$form->dropDownList(
								$estate,
								'customer_id',
								Xul::listData(Customer::model()->findAll(array('order'=>'name')), 'id', 'name')
								,array('id'=>Xul::uniqueId()))
				);
				
				echo Xul::endVbox();
				
			echo Xul::endGroupbox();
			
			// ---------------- Image -----------------------
			
			echo Xul::beginGroupbox(null, null, 'Image');
				
				echo Xul::beginVbox(array('flex'=>1));
				
					$fileUpload = $this->widget('ZoolFileUpload', array(
							'pickerFilter'=>'filterImages'
							));				
					
					$form->registerSingleInput($fileUpload->getTextboxId(), 'Estate[image]');
	
					// the image
					echo Xul::beginBox();
					
						$imageUrl = $estate->getFileUrl('large');
					
						$imageUrl = $estate->getFileUrl('normal');							
						if(!empty($imageUrl)){

							$popupPanelId = Xul::uniqueId() . 'panel-id-'. $estate->id;
							
							// image panel
							echo Xul::beginPanel($popupPanelId, array('backdrag'=>'true', 'titlebar'=>'normal', 'fade'=>'fast', 'close'=>'true',
									'label'=>$estate->description,
									'noautohide'=>'true'));
							
								echo XuL::image(Yii::app()->request->getHostInfo('') . $estate->getFileUrl('large') . '?'.time() , 'Estate',
										array('style'=>'max-height: 700px; max-width: 700px;'));

							echo Xul::endPanel();
							// end panel
						
							$imageUrl .= '?'. time(); // cache hack						
							echo XuL::image(Yii::app()->request->getHostInfo('') . $imageUrl , 'Estate',
									array('width'=>200, 'popup'=> $popupPanelId));
						
						}
					
					echo Xul::endBox();
			
				echo Xul::endVbox();
			
			echo Xul::endGroupbox();
			
			
			 
		echo Xul::endHbox();
			
		// ------------ end data panels
		
		echo Xul::beginHbox();
				
				echo $form->submit('Save',
						$estate->isNewRecord ? array() : array(
								'reRender'=>'own-estates-tabpanel',
								'onbeforesubmit'=>'window.currentTableIndex = Zool.byId("estatetable").currentIndex;',
								'oncomplete'=>'Zool.byId("estatedetailsdeck").selectedIndex = Zool.byId("estatetable").currentIndex = window.currentTableIndex; '
						),
						array('flex'=>6, 'accesskey'=>'S', 'class'=>'save')
				);
				
				if(!$estate->isNewRecord)
					echo Xul::linkButton('Delete',
						 $this->createAbsoluteUrl('estate/delete', array('id'=>$estate->id)),
						 array('reRender'=>'own-estates-tabpanel', 'confirm'=>true, 'confirmContent'=>'Are you sure you want to delete?'),
						 array('flex'=>1));
				
		echo Xul::endHbox();
		
	$this->endWidget();

echo Xul::endGroupbox();