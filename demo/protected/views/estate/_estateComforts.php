<?php
/* @var $estate Estate */
/* @var $form ZoolActiveForm */
/* @var $comfortDataProvider ComfortDataProvider */
$listOptions = array('id'=>'targetcomforts'.$estate->id);
$form->registerInput($estate, 'comforts', $listOptions);
?>

<richlistbox 
	id="<?php echo $listOptions['id']; ?>"
	seltype="multiple"
	rows="10"
	class="checkboxed"
	width="200"
	height="200"><?php 
	foreach ($comfortDataProvider->getData() as $comfort){					
		//echo Xul::listitem($comfort->name, $comfort->id, $estate->hasComfort($comfort));
		echo '<richlistitem value="'.$comfort->id.'">';
			echo Xul::checkBox(null, $comfort->name, $estate->hasComfort($comfort));
		echo "</richlistitem>";
	}
?></richlistbox>