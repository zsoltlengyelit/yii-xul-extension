<?php

/* @var $this EstateController */
/* @var $form ZoolActiveForm */
/* @var $dataDataProvider EstateDataProvider */

?>

<deck
    id="estatedetailsdeck"
    flex="1"><?php

    foreach ($dataProvider->getData() as $estate){

        $this->renderPartial('_form', array('estate'=>$estate, 'comfortDataProvider'=> $comfortDataProvider));

	}
?></deck>
