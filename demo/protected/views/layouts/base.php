<window
    title="<?php echo $this->pageTitle; ?>"
    width="1000"
    id="main"
    xmlns:svg="http://www.w3.org/2000/svg"
    xmlns:html="http://www.w3.org/1999/xhtml"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    height="800">
    
<?php

    Yii::app()->clientScript->registerInitScript('test', <<<SCRIPT

    	var progressbar = function(){
    		return Zool.byId('app-progressbar');
    	};
    		
    	var rightStatusPanel  = function(){
    		return Zool.byId('right-statusbar-panel');
    	};
    		
         App.beforeAjax = function(url, config){
    		if(progressbar())    		
    			progressbar().mode = 'undetermined';
    		
    		if(rightStatusPanel())
				rightStatusPanel().label = 'Loading...';
    	};
    		
    	App.afterAjax = function(url, config){
    		if(progressbar()){    		
    			progressbar().mode = 'determined';
    			progressbar().value = 0;
    		}
    		if(rightStatusPanel())
				rightStatusPanel().label = '';
    	};

SCRIPT
, CClientScript::POS_HEAD);
/*
 Yii::app()->clientScript->registerScript('test2', <<<SCRIPT

         alert('hello ready');

         SCRIPT
         , CClientScript::POS_READY);

*/
    Yii::app()->clientScript->registerInitCssFile(Yii::app()->baseUrl . '/css/xul.css');

?>

<!-- end of menu -->

<vbox flex="1">
    <hbox flex="1">

        <vbox flex="4" id="content">
            <?php echo $content;?>
        </vbox>

    </hbox>
</vbox>

<statusbar id="statusbar">
  <statusbarpanel id="left-statusbar-panel" label="Application loaded."/>
  <spacer flex="1" />
  <statusbarpanel id="right-statusbar-panel" label=""/>
  <progressmeter id="app-progressbar" mode="determined" value="0"/>  
</statusbar>

</window>
