<window
    title="<?php echo $this->pageTitle; ?>"
    width="1000"
    id="main"
    xmlns:svg="http://www.w3.org/2000/svg"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    height="800">
    
<?php

	//Yii::app()->clientScript->registerZooltag('application.userselector');

    Yii::app()->clientScript->registerInitScript('test', <<<SCRIPT

    	var progressbar = function(){
    		return Zool.byId('app-progressbar');
    	};
    		
    	var rightStatusPanel  = function(){
    		return Zool.byId('right-statusbar-panel');
    	};
    		
         App.beforeAjax = function(url, config){
    		if(progressbar())    		
    			progressbar().mode = 'undetermined';
    		
    		if(rightStatusPanel())
				rightStatusPanel().label = 'Loading...';
    	};
    		
    	App.afterAjax = function(url, config){
    		if(progressbar()){    		
    			progressbar().mode = 'determined';
    			progressbar().value = 0;
    		}
    		if(rightStatusPanel())
				rightStatusPanel().label = '';
    	};

SCRIPT
, CClientScript::POS_HEAD);

Yii::app()->clientScript->registerInitCssFile(Yii::app()->baseUrl . '/css/xul.css');

?>

<toolbox> 
  <toolbar id="nav-toolbar"><?php
           
		   // ingtlanokhoz navigálás
           echo Xul::beginToolbarbutton(' Estates', array('estate/index'), array(),
		   
						// ikon
           		array('image'=> Yii::app()->request->getBaseUrl(true) . '/images/estate_th.png',
					
						// CSS osztály, hogy jelezzük az aktív menüpontot
           			  'class' => Yii::app()->controller->id == 'estate' ? 'active' : '',
					  
					  // az ikon a szöveg mellett van
           			  'orient'=>'horizontal',
					  
					  // ez egy menüvel rendelkező gomb
					  'type'=>'menu-button'));
				
				// a gomb menüje
				echo Xul::beginMenupopup();					
					echo Xul::menuitem('New estate', Xul::action(array('estate/create')), array(), true);				
				echo Xul::endMenupopup();

		   echo Xul::endToolbarbutton(); 
           
		   // admin can manage users
		   if(Yii::app()->user->name == 'admin'){
		   
	            echo Xul::beginToolbarbutton(' Users', array('user/index'), array('data'=> 'tester=awesome&full=yes', 'method'=>'POST'),
	            		array('image'=> Yii::app()->request->getBaseUrl(true) . '/images/users_th.png',
						'class'=>Yii::app()->controller->id == 'user' ? 'active' : '',
						'orient'=>'horizontal',
						'type'=>'menu-button'));
					
					echo Xul::beginMenupopup();					
						echo Xul::menuitem('New user', Xul::action(array('user/create')), array(), true);				
					echo Xul::endMenupopup();
					
				echo Xul::endToolbarbutton();
			
			}
			
			// --------- Customers --------------
			
			echo Xul::beginToolbarbutton(' Customers', array('customer/index'), array('method'=>'GET'),
					array('image'=> Yii::app()->request->getBaseUrl(true) . '/images/customers_th.png',
							'class'=>Yii::app()->controller->id == 'customer' ? 'active' : '',
							'orient'=>'horizontal',
							'type'=>'menu-button'));
				
				echo Xul::beginMenupopup();
				echo Xul::menuitem('New customer', Xul::action(array('customer/create')), array(), true);
				echo Xul::endMenupopup();
				
			echo Xul::endToolbarbutton();

  
			echo Xul::toolbarbutton(' Settings', array('config/index'), array(),
				array('image'=> Yii::app()->request->getBaseUrl(true) . '/images/config_th.png',
				'class' => Yii::app()->controller->id == 'config' ? 'active' : ''));
                        
            echo Xul::spacer(1);    

            echo Xul::vbox(
					Xul::spacer(1).
					Xul::description('Logged as: ' . Yii::app()->user->name, array('valign'=>'center', 'class'=>'header', 'flex'=>1, 'style'=>'font-size: 1.3em;')),
					array('algin'=>'right'));

			echo Xul::tag('splitter', array('collapse'=>'none'));
			
            echo Xul::toolbarbutton('Logout ', array('site/logout'), array(), 
				array('image'=> Yii::app()->request->getBaseUrl(true) . '/images/logout_th.png',
				'dir'=>'reverse',
				'class'=>'logout'));

?></toolbar>
</toolbox>

<vbox flex="1">  
	<groupbox flex="1">      
        <vbox id="content">
            <?php echo $content;?>
        </vbox>
	</groupbox>
</vbox>

<statusbar id="statusbar">
  <statusbarpanel id="left-statusbar-panel" label="Application loaded."/>
  <spacer flex="1"/>
  <statusbarpanel id="right-statusbar-panel" label=""/>
  <progressmeter id="app-progressbar" mode="determined" value="0"/>  
</statusbar>

</window>
