<?php
    Yii::app()->clientScript->registerInitScript('test', <<<SCRIPT

    	var progressbar = function(){
    		return Zool.byId('app-progressbar');
    	};
    		
    	var rightStatusPanel  = function(){
    		return Zool.byId('right-statusbar-panel');
    	};
    		
         App.beforeAjax = function(url, config){
    		if(progressbar())    		
    			progressbar().mode = 'undetermined';
    		
    		if(rightStatusPanel())
				rightStatusPanel().label = 'Loading...';
    	};
    		
    	App.afterAjax = function(url, config){
    		if(progressbar()){    		
    			progressbar().mode = 'determined';
    			progressbar().value = 0;
    		}
    		if(rightStatusPanel())
				rightStatusPanel().label = '';
    	};

SCRIPT
, CClientScript::POS_HEAD);

Yii::app()->clientScript->registerInitCssFile(Yii::app()->baseUrl . '/css/xul.css');

?>

<window
    title="<?php echo $this->pageTitle; ?>"
    width="1000"
    id="main"
    xmlns:svg="http://www.w3.org/2000/svg"
    xmlns:h="http://www.w3.org/1999/xhtml"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    height="800">


<vbox flex="1" align="center">
    <hbox flex="1" align="center">
        <vbox flex="1" align="center">

        <hbox>
            <?php echo XuL::image(Yii::app()->getModule('zool')->serverUrl . '/images/estate.png', 'Estate Agency',
                    array('width'=>128, 'height'=>128, 'flex'=>1));?>
             <vbox>
                <hbox flex="1"></hbox>
                <description flex="1" class="h1" align="center"><?php echo Yii::app()->name; ?></description>
            </vbox>

        </hbox>

         <groupbox width="500" align="center">
             <caption label="Login" />
             <?php echo $content;?>
         </groupbox>

         </vbox>
    </hbox>
</vbox>

<statusbar id="statusbar">
  <statusbarpanel id="left-statusbar-panel" label="Application loaded."/>
  <spacer flex="1"/>
  <statusbarpanel id="right-statusbar-panel" label=""/>
  <progressmeter id="app-progressbar" mode="determined" value="0"/>  
</statusbar>

</window>
