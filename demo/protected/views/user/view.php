<?php

Yii::app()->clientScript->registerScript('mainwin', <<<SCRIPT


SCRIPT
,CClientScript::POS_LOAD);

echo Xul::beginVbox(array('flex'=>1));

$this->widget('ZoolDetailView', array(
		'data'=>$model,
		'htmlOptions'=>array(
				'flex'=>1,
				),
		'attributes'=>array(
				'username',
				'fullname',
				'email',
				'active',
				'agent'
				),
		));

	echo Xul::beginHbox();
	
		echo Xul::linkButton('Edit', $this->createAbsoluteUrl('user/edit',array('id'=>$model->id)), array(), array('flex'=>4));
		
		echo Xul::button('Close', "window.close()", array('flex'=>1));
		
		//echo Xul::button('Print', "Zool.byId('brow').print();", array('flex'=>1));
	
	echo Xul::endHbox();
	
//	echo '<html:iframe id="brow" src="'.$this->createAbsoluteUrl('site/print').'" onload="window.print();" />';
	
echo Xul::endVbox();