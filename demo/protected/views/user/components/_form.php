<?php
/* @var $this UserController */
/* @var $user User */
/* @var $form ZoolActiveForm */

echo Xul::beginGroupbox(null, 1, $user->isNewRecord ? 'New user' : 'Edit user');

	echo Xul::beginVbox(array('flex'=>1));
	
	
		
		$form = $this->beginWidget('ZoolActiveForm', array(
				'method'=>'POST',
				'autofocus'=>$user->isNewRecord ? 'usernameField' :null,
				'action'=> $user->isNewRecord ? $this->createAbsoluteUrl('user/create') : $this->createAbsoluteUrl('user/update', array('id'=>$user->id)),
		));
		
			echo Xul::beginVbox();
			
				?>
					<grid>
						<columns>
							<column />
							<column />
						</columns>
						
						<rows>
				<?php
				
				echo "<row>";
					echo Xul::vbox($form->textFieldRow($user, 'username', array('flex'=>1, 'id'=>$user->isNewRecord ? 'usernameField' : Xul::uniqueId())));			
					
					echo Xul::vbox($form->textFieldRow($user, 'email', array('id'=>Xul::uniqueId())));
				echo "</row>";
				
				echo "<row>";
					echo Xul::vbox($form->textFieldRow($user, 'fullname', array('id'=>Xul::uniqueId())));
					
					echo Xul::vbox($form->passwordFieldRow($user, 'password', array('id'=>Xul::uniqueId())));
				echo "</row>";
				
				echo "<row>";
					echo Xul::vbox($form->checkBoxRow($user, 'active', array('id'=>Xul::uniqueId())));
					
					echo Xul::vbox($form->checkBoxRow($user, 'agent', array('id'=>Xul::uniqueId())));
				echo "</row>";
				
				?>
					</rows>
				</grid>
				<?php
				
				// ------------- Profile image ------------------
				
				echo Xul::beginGroupbox(null, 1, 'Profile image', array('align'=>'center'));
				
					echo Xul::beginVbox(array('align'=>'center'));
					
						// the image
						echo Xul::beginBox(array('align'=>'center'));
							
						$imageUrl = $user->getFileUrl('thumb');							
						if(!empty($imageUrl)){

							$popupPanelId = Xul::uniqueId() . 'panel-id-'. $user->id;
							
							// image panel
							echo Xul::beginPanel($popupPanelId, array('backdrag'=>'true', 'titlebar'=>'normal', 'fade'=>'fast', 'close'=>'true',
									'label'=>$user->fullname,
									'noautohide'=>'true'));
							
								echo XuL::image(Yii::app()->request->getHostInfo('') . $user->getFileUrl('large') . '?'.time() , 'User',
										array('style'=>'max-height: 700px; max-width: 700px;'));

							echo Xul::endPanel();
							// end panel
						
							$imageUrl .= '?'. time(); // cache hack						
							echo XuL::image(Yii::app()->request->getHostInfo('') . $imageUrl , 'User',
									array('width'=>60, 'popup'=> $popupPanelId));
						
						}
							
						echo Xul::endBox();	

						$fileUpload = $this->widget('ZoolFileUpload', array(
								'pickerFilter'=>'filterImages'
						));
						$form->registerSingleInput($fileUpload->getTextboxId(), 'User[image]');
					
					echo Xul::endVbox();
					
				echo Xul::endGroupbox();
				
				
				//------------- end Profile image ------------------
							
				echo Xul::beginHbox();
				
					if($user->isNewRecord){
						echo $form->submit(' Save',array(),
								array('flex'=>1,
										'accesskey'=>'S',
										'image'=> Yii::app()->request->getBaseUrl(true) . '/images/save.png',)
						);
					}else{
						echo $form->submit('Save',
								array(
										'reRender'=>'user',
										'onbeforesubmit'=>'window.currentTableIndex = Zool.byId("usertable").currentIndex;',
										'oncomplete'=>'Zool.byId("userdetailsdeck").selectedIndex = Zool.byId("usertable").currentIndex = window.currentTableIndex; '
								),
								array('flex'=>6, 'accesskey'=>'S')
						);
					}
						
						if(!$user->isNewRecord){
							
							echo Xul::linkButton('Delete',
									$this->createAbsoluteUrl('user/delete', array('id'=>$user->id)),
									array(),
									array('flex'=>1));
				
							echo Xul::button('Dialog', 'Zool.openWindow("'. $this->createAbsoluteUrl('user/view', array('id'=>$user->id)) .'", false, 200, 300);');
							
						}
				echo Xul::endHbox();
			
			echo Xul::endVbox();
		
		$this->endWidget();
	
	echo Xul::endVbox();

echo Xul::endGroupbox();

echo Xul::box('', array('flex'=>1));
