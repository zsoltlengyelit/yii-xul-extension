<?php
/* @var $this UserController */
/* @var $userDataProvider UserDataProvider */
?>

<deck
    id="userdetailsdeck"
    flex="1"> <?php

    foreach ($dataProvider->getData() as $user){
		echo Xul::beginBox();
    	$this->renderPartial('components/_form', array('user'=>$user));  
		echo Xul::endBox();
	}
	?>

</deck>
