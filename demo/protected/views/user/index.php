<?php
/* @var $this SiteController */
/* @var $userDataProvider UserDataProvider */
/* @var $tabbox ZoolTabbox */

$this->pageTitle=Yii::app()->name;


echo Xul::beginVbox(array('id'=>'user-page','flex'=>1));


	
// -------------------- filter -------------------------------
echo Xul::beginHbox(array('flex'=>1));	
					
		$systemUsers = new UserDataProvider();
		
		// search form
		$searchForm = $this->beginWidget('ZoolActiveForm', array(
				'id'=>'user-search',
				'submitConfig'=>
				array('reRender'=>'user-page'),
		));
					
		echo $searchForm->textField($systemUsers->getFilter(), 'filter', array(
				'emptytext'=>'Search...',
				'flex'=>1, 'id'=>Xul::uniqueId()));
			
		$this->endWidget();
		
		echo Xul::spacer(1);
		
		echo Xul::linkButton('Refresh', $this->createAbsoluteUrl('user/index'), array('reRender'=>'user-page'));

echo Xul::endHbox();

// ----------------------- grid ---------------------------

echo Xul::beginHbox(array('flex'=>1));
	
	$this->widget('ZoolGridView', array(
			'id'=>'user',
			'dataProvider'=>$systemUsers,
			'treeOptions'=>array('id'=>'usertable'),
			'htmlOptions'=>array('flex'=>2),
			'summaryText'=> '',
			'onselect' => "Zool.byId('userdetailsdeck').selectedIndex = this.currentIndex;",
			'pager'=>array(
					'reRender'=>'userdetailsdeck'
			),
			'columns'=>array('username', 'fullname', 'email',
					array(
						'name'=>'agent',
						'type'=>'raw',
						'value'=>'$data->agent == 1 ? "Yes" : "No"',
					))
	));
	
	
	$this->renderPartial('components/userDetails', array('dataProvider'=>$systemUsers));


echo Xul::endHbox();


echo Xul::endVbox();
