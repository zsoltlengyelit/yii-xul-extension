<?php
/* @var $this ConfigController */
/* @var $newComfort Comfort */
/* @var $addForm ZoolActiveForm */

?>
<hbox>

		
<groupbox id="add-comfort-groupbox">
<caption label="New comfort" />
	
	
	<?php 
	
	$addForm = $this->beginWidget('ZoolActiveForm', array(
		'method'=>'POST',
		'xmlOptions'=>array(),
		'tagName'=>'vbox',
		'submitConfig'=>array('reRender'=>'comforts-groupbox,add-comfort-groupbox'), 
		'action'=> $this->createAbsoluteUrl('config/createComfort'),
	));

		echo $addForm->textFieldRow($newComfort, 'name');
		
		echo $addForm->submit('Save');
	
	$this->endWidget();
	
	
	?>
</groupbox>


<groupbox id="comforts-groupbox" >
	<caption label="Comforts" />
	
	<box style="max-height: 500px; overflow: auto; min-width: 300px;">

		<grid>
			<columns>
				<column />
				<column />
			</columns>
			
			<rows>
			<?php 
			
				$comforts = Comfort::model()->findAll();
				
				if(empty($comforts)){
					echo Xul::description('There is no comfort in the system. Please add at least some.');
				}else			
					foreach ($comforts as $comfort){
					?>
					<row>
						<description value="<?php echo $comfort->name; ?>" />						
						<?php echo Xul::linkButton('Delete', array('/comfort/delete', 'id'=>$comfort->id), 
								array('reRender'=>'comforts-groupbox', 'confirm'=>true)); ?>
					</row>
					<?php
				}
			
			?>
			</rows>
			
		</grid>
		
	</box>
	
</groupbox>



</hbox>
