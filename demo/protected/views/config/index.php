<?php
/* @var $this ConfigController */
/* @var $tabbox ZoolTabbox */

$tabbox = $this->beginWidget('ZoolTabbox', array(
		'id'=>'config-tabs',
		'ajaxRendered'=>true,
		'tabboxOptions'=>array('flex'=>1),
));

if($tabbox->beginTab('Comforts', 'comforts-tab', array(
		'tabpanelId'=>'comforts-tabpanel',
		'ajaxRendered'=>false,
))){
	
	
	
	$this->renderPartial('_comforts', 
			array('newComfort'=>isset($newComfort) ? $newComfort : new Comfort()));
	
	$tabbox->endTab();
}

// if($tabbox->beginTab('Upload', 'upload-tab', array(
// 		'tabpanelId'=>'upload-tabpanel',
// 		'ajaxRendered'=>false,
// ))){



// 	$tabbox->endTab();
// }

$this->endWidget();
		