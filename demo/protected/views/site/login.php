<?php
/* @var $this SiteController */
/* @var $form ZoolActiveForm */


// a form definíciója. Akció, metódus...
$form = $this->beginWidget('ZoolActiveForm', array(
        'action'=>'site/login',
        'method'=>'POST',
        'autofocus'=> 'LoginForm[username]', // ez a mező automatikusan kurzor fókuszt kap
));

	// egy egyszerű dobox amivel egymás alatt jeleníttetjük meg az elemeket
	echo Xul::beginVbox(array('id'=>'loginHolder'));

		// felhasználó név
		echo Xul::vbox($form->textFieldRow($model, 'username'));

		// jelszó
		echo Xul::vbox($form->passwordFieldRow($model, 'password'));

		// emlékezzen a rendszer a felhasználó névre?
		echo Xul::vbox($form->checkBoxRow($model, 'rememberMe'));

		// az űrlap elküldéséhez szükséges gomb
		echo Xul::vbox($form->submit('Login'));

	echo Xul::endVbox();

$this->endWidget();


