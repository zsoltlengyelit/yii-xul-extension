<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return  array(
		'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
		'name'=>'My Console Application',

		// preloading 'log' component
		'preload'=>array('log'),

		// application components
		'components'=>array(

				'db'=>array(
						'connectionString' => 'mysql:host=localhost;dbname=agency',
						'emulatePrepare' => true,
						'username' => 'root',
						'password' => '',
						'charset' => 'utf8',
				),
					
				'log'=>array(
						'class'=>'CLogRouter',
						'routes'=>array(
								array(
										'class'=>'CFileLogRoute',
										'levels'=>'error, warning',
								),
						),
				),
		),

		'params'=>array(
				// this is used in contact page
				'adminEmail'=>'webmaster@example.com',
				'salt'=> 'g;};54#@&f5bfegoergp6eorgpe649Q5ggerg'
		),
);