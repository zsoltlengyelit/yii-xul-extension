<?php

/**
 *
 * @author Zsolt Lengyel
 *
 */
class ConfigController extends ZoolController{

	public $layout = 'zool';

	public function zoolActions(){
		return array('*');
	}

	/**
	 *
	 */
	public function actionIndex(){
		$this->render('index');
	}

	public function actionCreateComfort(){

		$newComfort = new Comfort();

		if(isset($_POST['Comfort'])){
			$newComfort->attributes = $_POST['Comfort'];
				
			if($newComfort->save()){
				$newComfort = new Comfort(); // can create new again
			}else{
				Yii::app()->user->setFlash('error', 'Couldn\'t save comfort.');
			}
		}

		$this->render('index', array('newComfort'=>$newComfort));
	}
}