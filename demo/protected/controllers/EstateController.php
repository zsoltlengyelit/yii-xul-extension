<?php

/**
 * 
 * @author Zsolt Lengyel
 *
 */
class EstateController extends ZoolController{
	
	public $layout = 'zool';
	
	public function zoolActions(){
		return array('*');
	}
	
	public function actionIndex() {
				
		$this->render('index', 
				array('comfortDataProvider' => new ComfortDataProvider()));
		
	}
	
	public function actionCreate(){
		
		$model = new Estate();
		
		// agent is the current user
		$model->agent_id = Yii::app()->user->id;
		
		if(isset($_POST['Estate'])){
			$model->attributes = $_POST['Estate'];
			$model->sold = $_POST['Estate']['sold'];
			
			if(isset($_POST['Estate']['comforts']) && !empty($_POST['Estate']['comforts'])){
				$model->comforts = explode(',', $_POST['Estate']['comforts']);			
			}
			
			if($model->save()){
				Yii::app()->user->setFlash('success', "Estate {$model->title} saved.");
				$this->forward('index');
			}
		}
		
		$this->render('create', 
				array('estate'=>$model,
					  'comfortDataProvider' => new ComfortDataProvider()));
		
	}
	
	/**
	 * 
	 * @param integer $id
	 */
	public function actionEdit($id){
		
		$this->render('edit', array(
				'model'=>$this->loadModel($id)
				));	
	}
	
	/**
	 *
	 * @param integer $id
	 */
	public function actionUpdate($id){
	
		$model = $this->loadModel($id);
	
		if(isset($_POST['Estate'])){
			$model->attributes = $_POST['Estate'];
			$model->sold = $_POST['Estate']['sold'];
			
			if(isset($_POST['Estate']['comforts']) && !empty($_POST['Estate']['comforts'])){
				$model->comforts = explode(',', $_POST['Estate']['comforts']);			
			}
			
			if($model->save()){
				
				Yii::app()->user->setFlash('success', "Estate {$model->title} saved.");
				
			}else{
				
				Yii::app()->user->setFlash('Validation failed', Xul::formatErrors($model));
				return;
			}
	
		}else{
			Yii::app()->user->setFlash('Validation failed', 'Missing data');
		}
	
		$this->forward('estate/index');
	
	}
	
	/**
	 *
	 * @param integer $id
	 */
	public function actionDelete($id){
	
		if(Estate::model()->deleteByPk($id)){
			Yii::app()->user->setFlash('success', "Estate deleted.");
		}else{
			Yii::app()->user->setFlash('error', 'Error while delete estate.');
		}
	
		$this->forward('estate/index');
	
	}
	
	/**
	 *
	 * @param integer $id
	 */
	public function actionView($id){
		 
		$model = $this->loadModel($id);
		 
		if(null === $model){
			Yii::app()->user->setFlash('error', 'Cannot find estate.');
		}else{
	
			$this->render('view', array('model'=>$model));
	
		}
		 
		 
	}
	
	/**
	 * 
	 * @param integer $id
	 * @return CModel
	 */
	protected function loadModel($id){
		
		$estate = Estate::model()->findByPk($id);
		
		return $estate;
		
	}
	
}