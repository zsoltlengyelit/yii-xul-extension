<?php

/**
 * 
 * @author Zsolt Lengyel
 *
 */
class CustomerController extends ZoolController{
	
	public $layout = 'base';
	
	public function zoolActions(){
		return array('*');
	}
	
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		$dataProvider = new CustomerDataProvider();
	
		$this->layout = 'zool';
		$this->render('index', array('dataProvider'=> $dataProvider));
	}
	
	public function actionEdit($id){
			
		$model = Customer::model()->findByPk($id);
	
		if(null === $model){
			Yii::app()->user->setFlash('error', 'Missing data');
		}else{
	
			$this->render('edit', array('model'=>$model));
		}
			
	}
	
	/**
	 *
	 *
	 */
	public function actionCreate(){
			
		$this->layout = 'zool';
		
		$model = new Customer();
	
		if(isset($_POST['Customer'])){
			$model->attributes = $_POST['Customer'];
			
			if($model->save()){
				Yii::app()->user->setFlash('success', "Customer saved.");
				$this->forward('index');
			}else{
								
				Yii::app()->user->setFlash('Validation failed', Xul::formatErrors($model));
			}
		}
	
		$this->render('create', array('model'=>$model));
	}
	
	/**
	 *
	 * @param integer $id
	 */
	public function actionUpdate($id){
	
		$model = Customer::model()->findByPk($id);
	
		if(isset($_POST['Customer'])){
			$model->attributes = $_POST['Customer'];
				
		if($model->save()){
				Yii::app()->user->setFlash('success', "Customer saved.");
				$this->forward('index');
			}else{
								
				Yii::app()->user->setFlash('Validation failed', Xul::formatErrors($model));
			}
	
		}else{
			Yii::app()->user->setFlash('error', 'Missing data');
		}
	
		$this->forward('index');
	
	}
	
	/**
	 *
	 * @param integer $id
	 */
	public function actionDelete($id){
	
		try{
			if(Customer::model()->deleteByPk($id)){
				Yii::app()->user->setFlash('success', 'Customer deleted.');
			}else{
				Yii::app()->user->setFlash('error', 'Error while delete customer.');
			}
		}catch (CDbException $e){
			Yii::app()->user->setFlash('error', 'Error while delete customer: ' . $e->getMessage());
		}
	
		$this->forward('index');
	
	}

	
	
}