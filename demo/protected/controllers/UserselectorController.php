<?php

/**
 * 
 * @author Zsolt Lengyel
 *
 */
class UserselectorController extends CController{
	

	public function actionSuggest($filter = ''){
		 
		if(strlen($filter) < 3){
			echo ZoolJSON::encode(array());
			Yii::app()->end();
		}
		 
		$criteria = new CDbCriteria();
		$criteria->addSearchCondition('username', $filter);
		 
		$userModels = User::model()->findAll($criteria);
		 
		$users = array();
		 
		foreach ($userModels as $user){
			$users[] = array('name'=>$user->fullname, 'id'=>$user->id);
		}
		 
		echo ZoolJSON::encode($users);
	}
	
}