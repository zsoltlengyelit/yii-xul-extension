<?php

/**
 * 
 * @author Zsolt Lengyel
 *
 */
class ComfortController extends ZoolController{
	
	/**
	 * 
	 * @param integer $id
	 */
	public function actionDelete($id){
		
		try{
			if(!Comfort::model()->deleteByPk($id)){
				Yii::app()->user->setFlash('error', 'Couldn\'t remove comfort: '. $this->load($id)->name);
			}
		}catch (CDbException $e){
			Yii::app()->user->setFlash('error', 'Couldn\'t remove comfort: '. $this->load($id)->name);
		}

		$this->forward('/config');
		
	}

	
	private function load($id){		
		return Comfort::model()->findByPk($id);
	}
	
}