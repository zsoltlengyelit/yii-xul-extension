<?php

/**
 *
 * @author Zsolt Lengyel
 *
 */
class UserController extends ZoolController{

	public $layout = 'base';

	public function zoolActions(){
		return array('*',
				'except'=>'suggest');
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		$dataProvider = new UserDataProvider();

		$this->layout = 'zool';
		$this->render('index', array('userDataProvider'=> $dataProvider));
	}

	public function actionEdit($id){
		 
		$user = User::model()->findByPk($id);

		if(null === $user){
			Yii::app()->user->setFlash('error', 'Missing data');
		}else{

			$this->render('edit', array('user'=>$user));
		}
		 
	}

	/**
	 *
	 *
	 */
	public function actionCreate(){
		 
		$this->layout = 'zool';
		 
		Yii::log('Start to create new user.', CLogger::LEVEL_TRACE, 'zool');

		$user = new User;

		if(isset($_POST['User'])){
			$user->attributes = $_POST['User'];
			$user->setAttribute('agent', $_POST['User']['agent']);
			$user->setAttribute('active', $_POST['User']['active']);

			Yii::log('User agent: ' + print_r($user->agent, true), CLogger::LEVEL_TRACE, 'zool');

			if($user->save()){
				Yii::app()->user->setFlash('success', "User {$user->username} saved.");
				$this->forward('index');
			}else{
				 
				Yii::app()->user->setFlash('error', "Validation failed.");
				 
				foreach ($user->getErrors() as $error)
					Yii::app()->user->setFlash('error', $error);
			}
		}

		$this->render('create', array('user'=>$user));
	}

	/**
	 *
	 * @param integer $id
	 */
	public function actionUpdate($id){

		$user = User::model()->findByPk($id);

		if(isset($_POST['User'])){
			$user->attributes = $_POST['User'];
			$user->setAttribute('agent', $_POST['User']['agent']);
			$user->setAttribute('active', $_POST['User']['active']);
			
			$user->save();

			Yii::app()->user->setFlash('success', "User {$user->username} saved.");

		}else{
			Yii::app()->user->setFlash('error', 'Missing data');
		}

		$this->forward('index');

	}

	/**
	 *
	 * @param integer $id
	 */
	public function actionDelete($id){

		try{
			if(User::model()->deleteByPk($id)){
				Yii::app()->user->setFlash('success', "User deleted.");
			}else{
				Yii::app()->user->setFlash('error', 'Error while delete user.');
			}
		}catch (CDbException $e){
			Yii::app()->user->setFlash('error', 'Error while delete user');
		}

		$this->forward('index');

	}

	/**
	 *
	 * @param integer $id
	 */
	public function actionView($id){
		 
		$model = User::model()->findByPk($id);
		 
		if(null === $model){
			Yii::app()->user->setFlash('error', 'Cannot find user.');
		}else{

			$this->render('view', array('model'=>$model));

		}
	}
	 

}