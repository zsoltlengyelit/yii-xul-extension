<?php

/**
 * Simple JSON parser. Do not implement any algorithm, just uses the native encode if exists. Otherwise uses the CJSON class.
 *
 * @author Zsolt Lengyel
 *
 */
class ZoolJSON {

    /**
     *
     * @param mixed $variable
     * @param bool $replaceQuotes true if we want to escape " charaters
     * @return string|Ambigous <string, number, mixed>
     */
    public static function encode($variable, $replaceQuotes = false){

        $result = "";

        if(function_exists('json_encode')){
            $result = json_encode($variable, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT );
        }else
            $result = CJSON::encode($variable);

//         if($replaceQuotes){
//             $result = str_replace('"', '\"', $result);
//         }

        return $result;

    }


}