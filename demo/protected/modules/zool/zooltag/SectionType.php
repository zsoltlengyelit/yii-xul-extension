<?php

/**
 * Type of section in zooltag (custom XUL element).
 * 
 * @author Zsolt Lengyel
 *
 */
class SectionType {
	const XML = 0;
	const JS = 1;
	const XUL = 2;
}