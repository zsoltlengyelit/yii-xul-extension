<?php


class ZooltagField extends ZooltagComponent{
	
	private $name;
	
	private $readonly = false;
	
	public function __construct($name, $readonly = false){
		$this->name = $name;
		$this->readonly = $readonly;
	}
	
	public function getName(){
		return $this->name;
	}

	protected function setName($name){
		$this->name = $name;
	}
	
	public function getReadonly(){
		return $this->readonly;
	}
	
	public function render(){
	
		echo Xul::openTag($this->getXblName('field'),
				array('name'=> $this->name,
					'readonly'=>$this->readonly
				));
	
		echo 'this.' . $this->name;
			
		echo Xul::closeTag($this->getXblName('field'));
	}
	
}