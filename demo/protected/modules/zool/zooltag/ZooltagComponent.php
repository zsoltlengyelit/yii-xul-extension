<?php

abstract class ZooltagComponent extends CComponent{
	
	
	/**
	 *
	 * @param string $tagname
	 * @return string
	 */
	protected function getXblName($tagname){
		return Yii::app()->getModule('zool')->xblNamespaceName . ':' . $tagname;
	}

	abstract public function render();
	
}