<?php

/**
 * 
 * @author Zsolt Lengyel
 *
 */
class ZooltagMethod extends ZooltagComponent{
	
	private $parameters = array();
	
	private $body = '';
	
	private $name = '';
	
	public function __construct($name, $parameters = array()){
		$this->name = $name;		
		$this->parameters = $parameters;
	}
	
	public function beginBody(){
		ob_start();		
	}
	
	public function endBody(){
		$this->body = ob_get_clean();
	}
	
	public function setBody($body){
		$this->body = $body;
	}
	
	public function render(){
		
		echo Xul::openTag($this->getXblName('method'), array('name'=>$this->name));
			
			foreach ($this->parameters as $parameter){
				echo Xul::tag($this->getXblName('parameter'), array('name'=>$parameter)); 
			}
			
			echo Xul::openTag($this->getXblName('body'));
				echo Xul::cdata($this->body);
			echo Xul::closeTag($this->getXblName('body'));			
		
		echo Xul::closeTag($this->getXblName('method'));
		
	}
	
	
}