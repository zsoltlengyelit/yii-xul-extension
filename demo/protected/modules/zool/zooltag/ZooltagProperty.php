<?php

/**
 * 
 * @author Zsolt Lengyel
 *
 */
class ZooltagProperty extends ZooltagField{
	
	
	/**
	 * 
	 * @var string JS getter
	 */
	private $getter = '';
	
	/**
	 * 
	 * @var string JS setter
	 */
	private $setter = '';
	
	/**
	 * 
	 * @param unknown_type $name
	 */
	public function __construct($name, $readonly = false){
		parent::__construct($name, $readonly);
	}
	
	// setter
	public function beginSetter(){
		ob_start();
	}	
	public function endSetter(){
		$this->setSetter(ob_get_clean());
	} 
	
	// getter
	public function beginGetter(){
		ob_start();
	}
	public function endGetter(){
		$this->setGetter(ob_get_clean());
	}
	
	public function setGetter($getter){
		$this->getter = trim($getter);
	}
	
	public function setSetter($setter){
		$this->setter = trim($setter);
	}
	
	public function getGetter(){
		$this->getter;
	}
	
	public function getSetter(){
		$this->setter;
	}
	
	public function render(){
		
		echo Xul::openTag($this->getXblName('property'), 
				array('name'=> $this->name,
				      'readonly'=> $this->readonly
						)) ."\n";
		
			if(!empty($this->getter)){
				echo Xul::openTag($this->getXblName('getter'));
					echo Xul::cdata($this->getter);
				echo Xul::closeTag($this->getXblName('getter')) ."\n";
			}
			
			if(!empty($this->setter)){
				echo Xul::openTag($this->getXblName('setter'));
					echo Xul::cdata($this->setter);
				echo Xul::closeTag($this->getXblName('setter')) ."\n";
			}
			
		echo Xul::closeTag($this->getXblName('property')) ."\n";
	}

	
}