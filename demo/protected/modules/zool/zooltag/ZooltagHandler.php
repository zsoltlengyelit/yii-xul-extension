<?php

/**
 * 
 * @author Zsolt Lengyel
 *
 */
class ZooltagHandler extends ZooltagComponent{
		
	private $options = array();
	
	private $content = '';
	
	/**
	 * 
	 * @param array $options
	 */
	public function __construct($options = array()){
		$this->options = $options;
	}
	
	public function begindBody(){
		ob_start();
	}
	
	public function endBody(){
		$this->body = ob_get_clean();
	}
	
	public function render(){
		
		echo Xul::openTag($this->getXblName('handler'), $this->options);
		
			echo Xul::cdata($this->content);
		
		echo Xul::closeTag($this->getXblName('handler'));		
	}
	
}