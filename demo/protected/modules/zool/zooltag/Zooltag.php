<?php

/**
 * 
 * @author Zsolt Lengyel
 *
 */
class Zooltag extends ZooltagComponent{
	
	private $name = null;
	
	/**
	 * 
	 * @var string
	 */
	private $content = '';
	
	/**
	 * 
	 * @var array
	 */
	private $properities = array();
	
	/**
	 * 
	 * @var ZooltagField[]
	 */
	private $fields = array();
	
	/**
	 * 
	 * @var unknown_type
	 */
	private $handlers = array();
	
	private $constructor = '';
	
	private $destructor = '';
	
	private $methods = array();
	
	private $options = array();
	
	private $implements = null;
		
	/**
	 * 
	 * @param string $name
	 * @param array $options
	 * @param string $bindingNamespace
	 */
	public function __construct($name, $options = array(), $implements = null){
		
		$this->name = $name;
		$this->options = $options;
		$this->implements = $implements;		
		
	}
	
	public function createProperty($name, $readonly = false){
		return $this->properities[] = new ZooltagProperty($name, $readonly);
	}
	
	public function createMethod($name, $parameters = array()){
		return $this->methods[] = new ZooltagMethod($name, $parameters);
	}
	
	public function createField($name, $parameters = array()){
		return $this->fields[] = new ZooltagField($name, $parameters);
	}
	
	public function createHandler($options = array()){
		return $this->handlers[] = new ZooltagHandler($options);
	}
		
	public function beginConstructor(){
		ob_start();
	}	
	public function endConstructor(){
		$this->constructor = ob_get_clean();
	}
	
	public function beginDestructor(){
		ob_start();
	}
	public function endDestructor(){
		$this->destructor = ob_get_clean();
	}
	
	public function beginContent(){
		ob_start();
	}
	
	public function endContent(){
		$this->content = ob_get_clean();
	}
	
	public function includeChildren($tagnames){
		echo Xul::tag($this->getXblName('children'), array('includes'=>$tagnames));
	}

	/**
	 * Renders binding.
	 */
	public function render(){
		
		$options = $this->options;
		$options['id'] = $this->name;
		
		echo Xul::openTag($this->getBindingName(), $options);
			
			// content
			echo Xul::openTag($this->getXblName('content')) ."\n";
				echo $this->content;
			echo Xul::closeTag($this->getXblName('content')) ."\n";
			
			// implementation
			echo Xul::openTag($this->getXblName('implementation'), array('implements'=>$this->implements)) ."\n";
				
				$this->renderConstructor();
				$this->renderDestructor();
				$this->renderFields();
				$this->renderProperties();
				$this->renderMethods();
				
			echo Xul::closeTag($this->getXblName('implementation')) ."\n";
			
			$this->renderHandlers();
		
		echo Xul::closeTag($this->getBindingName());
		
	}
	
	
	protected function renderConstructor(){
		if(!empty($this->constructor)){

			echo Xul::openTag($this->getXblName('constructor'));
				echo Xul::cdata($this->constructor);
			echo Xul::closeTag($this->getXblName('constructor')) ."\n";			
		}	
	}
	
	protected function renderDestructor(){
		if(!empty($this->destructor)){
	
			echo Xul::openTag($this->getXblName('destructor'));
			echo Xul::cdata($this->destructor);
			echo Xul::closeTag($this->getXblName('destructor')) ."\n";				
		}
	}
	
	
	protected function renderProperties(){
		foreach ($this->properities as $property) {
			$property->render();
		}		
	}
	
	protected function renderFields(){
		foreach ($this->fields as $field){
			$field->render();
		}
	}
	
	protected function renderHandlers(){
		if(!empty($this->handlers)){
			
			echo Xul::openTag($this->getXblName('handlers'));
				
				foreach ($this->handlers as $handler) {
					$handler->render();
				}
			
			echo Xul::closeTag($this->getXblName('handlers'));
			
		}
	}
	
	protected function renderMethods(){
		foreach ($this->methods as $method){
			$method->render();
		}
	}
	
	private function getBindingName(){
		return $this->getXblName('binding');
	}
	
	/**
	 * 
	 * @param string $tagname
	 * @return array, 0: lib name, 1: tagname
	 */
	public static function info($tagname){
		
		return explode('.', $tagname);
		
	}

	
}