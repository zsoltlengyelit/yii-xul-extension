<?php

Yii::import('zii.widgets.grid.CGridView');

/**
 *
 * @author Zsolt Lengyel
 *
*/
class ZoolGridView extends CGridView{

    /**
     *
     * @var integer number of displayed rows.
     */
    public $rows = 10;

    public $pager = array('class'=>'ZoolButtonPager');

    /**
     *
     * @var
     */
    public $treeOptions = array();

    public $onselect = null;
    
    /**
     * @var string the text to be displayed in a data cell when a data value is null. This property will NOT be HTML-encoded
     * when rendering. Defaults to an HTML blank.
     */
    public $nullDisplay='';
    /**
     * @var string the text to be displayed in an empty grid cell. This property will NOT be HTML-encoded when rendering. Defaults to an HTML blank.
     * This differs from {@link nullDisplay} in that {@link nullDisplay} is only used by {@link CDataColumn} to render
     * null data values.
     * @since 1.1.7
     */
    public $blankDisplay='';

    /**
     *
     * @var string name of wrapper node
     */
    public $tagName = 'vbox';

    /**
     * Initializes the grid view.
     * This method will initialize required property values and instantiate {@link columns} objects.
     */
    public function init()
    {

        $this->htmlOptions['id'] = $this->getId();

        if($this->onselect !== null){
            $this->treeOptions['onselect'] = $this->onselect;
        }

        $this->rows = $this->dataProvider->getPagination()->getPageSize();

        if(empty($this->updateSelector))
            throw new CException(Yii::t('zii','The property updateSelector should be defined.'));

        if(!isset($this->htmlOptions['class']))
            $this->htmlOptions['class']='grid-view';

        // 		if($this->baseScriptUrl===null)
        // 			$this->baseScriptUrl=Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('zii.widgets.assets')).'/gridview';

        // 		if($this->cssFile!==false)
        // 		{
        // 			if($this->cssFile===null)
        // 				$this->cssFile=$this->baseScriptUrl.'/styles.css';
        // 			Yii::app()->getClientScript()->registerCssFile($this->cssFile);
        // 		}

        $this->initColumns();
    }

    /**
     * Renders the view.
     * This is the main entry of the whole view rendering.
     * Child classes should mainly override {@link renderContent} method.
     */
    public function run()
    {
        // $this->registerClientScript();
        //parent::run();

        echo Xul::openTag($this->tagName,$this->htmlOptions)."\n";

        $this->renderContent();
        $this->renderKeys();

        echo Xul::closeTag($this->tagName) ."\n";
    }

    /**
     * Renders the key values of the data in a hidden tag.
     */
    public function renderKeys()
    {
        // TODO
        //         echo CHtml::openTag('box',array(
        //                 'class'=>'keys',
        //                 'style'=>'display:none',
        //                 'title'=>Yii::app()->getRequest()->getUrl(),
        //         ));
        //         foreach($this->dataProvider->getKeys() as $key)
            //             echo "<span>".CHtml::encode($key)."</span>";
            //         echo Xul::closeTag('box');
    }

    /**
     * Creates column objects and initializes them.
     */
    protected function initColumns()
    {
        if($this->columns===array())
        {
            if($this->dataProvider instanceof CActiveDataProvider)
                $this->columns=$this->dataProvider->model->attributeNames();
            else if($this->dataProvider instanceof IDataProvider)
            {
                // use the keys of the first row of data as the default columns
                $data=$this->dataProvider->getData();
                if(isset($data[0]) && is_array($data[0]))
                    $this->columns=array_keys($data[0]);
            }
        }
        $id=$this->getId();
        foreach($this->columns as $i=>$column)
        {
            if(is_string($column))
                $column=$this->createDataColumn($column);
            else
            {
                if(!isset($column['class']))
                    $column['class']='ZoolDataColumn';
                $column=Yii::createComponent($column, $this);
            }
            if(!$column->visible)
            {
                unset($this->columns[$i]);
                continue;
            }
            if($column->id===null)
                $column->id=$id.'_c'.$i;
            $this->columns[$i]=$column;
        }

        foreach($this->columns as $column)
            $column->init();
    }

    /**
     * Creates a {@link CDataColumn} based on a shortcut column specification string.
     * @param string $text the column specification string
     * @return CDataColumn the column instance
     */
    protected function createDataColumn($text)
    {
        if(!preg_match('/^([\w\.]+)(:(\w*))?(:(.*))?$/',$text,$matches))
            throw new CException(Yii::t('zool','The column must be specified in the format of "Name:Type:Label", where "Type" and "Label" are optional.'));
        $column=new ZoolDataColumn($this);
        $column->name=$matches[1];
        if(isset($matches[3]) && $matches[3]!=='')
            $column->type=$matches[3];
        if(isset($matches[5]))
            $column->header=$matches[5];
        return $column;
    }

    /**
     * Renders the data items for the grid view.
     */
    public function renderItems()
    {
        if($this->dataProvider->getItemCount()>0 || $this->showTableOnEmpty)
        {

            echo Xul::openTag('tree', CMap::mergeArray($this->treeOptions, array(
                    'class'=>$this->itemsCssClass,
                    'rows'=>$this->rows
            )))."\n";

            $this->renderTableHeader();
            ob_start();
            $this->renderTableBody();
            $body=ob_get_clean();
            $this->renderTableFooter();
            echo $body; // TFOOT must appear before TBODY according to the standard.
            echo Xul::closeTag('tree')."\n";
        }
        else
            $this->renderEmptyText();
    }

    /**
     * Renders the table header.
     */
    public function renderTableHeader()
    {
        if(!$this->hideHeader)
        {
            echo Xul::openTag('treecols')."\n";

            // TODO filtes
            //             if($this->filterPosition===self::FILTER_POS_HEADER)
            //                 $this->renderFilter();

            foreach($this->columns as $column){
                $column->renderHeaderCell();
                echo '<splitter class="tree-splitter"/>';
            }

            //             if($this->filterPosition===self::FILTER_POS_BODY)
            //                 $this->renderFilter();

            echo Xul::closeTag('treecols')."\n";
        }
        // TODO
//         else if($this->filter!==null && ($this->filterPosition===self::FILTER_POS_HEADER || $this->filterPosition===self::FILTER_POS_BODY))
//         {
//             echo "<thead>\n";
//             $this->renderFilter();
//             echo "</thead>\n";
//         }
    }

    /**
     * Renders the filter.
     * @since 1.1.1
     */
    public function renderFilter()
    {
        // TODO XUL style
        if($this->filter!==null)
        {
            echo "<tr class=\"{$this->filterCssClass}\">\n";
            foreach($this->columns as $column)
                $column->renderFilterCell();
            echo "</tr>\n";
        }
    }

    /**
     * Renders the table footer.
     */
    public function renderTableFooter()
    {
        $hasFilter=$this->filter!==null && $this->filterPosition===self::FILTER_POS_FOOTER;
        $hasFooter=$this->getHasFooter();
        if($hasFilter || $hasFooter)
        {
            echo "<tfoot>\n";
            if($hasFooter)
            {
                echo "<tr>\n";
                foreach($this->columns as $column)
                    $column->renderFooterCell();
                echo "</tr>\n";
            }
            if($hasFilter)
                $this->renderFilter();
            echo "</tfoot>\n";
        }
    }

    /**
     * Renders the table body.
     */
    public function renderTableBody()
    {
        $data=$this->dataProvider->getData();
        $n=count($data);
        echo Xul::openTag('treechildren')."\n";

        if($n>0)
        {
            for($row=0;$row<$n;++$row)
                $this->renderTableRow($row);
        }
        else
        {
        	
        	echo Xul::openTag('treeitem')."\n";
        	echo Xul::openTag('treerow')."\n";
            echo Xul::openTag('treecell')."\n";
        	
        	$this->renderEmptyText();
        	echo Xul::closeTag('treecell')."\n";
            echo Xul::closeTag('treerow')."\n";
       		echo Xul::closeTag('treeitem')."\n";
        }
        echo Xul::closeTag('treechildren')."\n";
    }

    /**
     * Renders a table body row.
     * @param integer $row the row number (zero-based).
     */
    public function renderTableRow($row)
    {

        echo Xul::openTag('treeitem')."\n";
        echo Xul::openTag('treerow')."\n";

        if($this->rowCssClassExpression!==null)
        {
            $data=$this->dataProvider->data[$row];
            $class=$this->evaluateExpression($this->rowCssClassExpression,array('row'=>$row,'data'=>$data));
        }
        else if(is_array($this->rowCssClass) && ($n=count($this->rowCssClass))>0)
            $class=$this->rowCssClass[$row%$n];
        else
            $class='';

        foreach($this->columns as $column)
            $column->renderDataCell($row);

        echo Xul::closeTag('treerow')."\n";
        echo Xul::closeTag('treeitem')."\n";

    }

    /**
     * Renders the empty message when there is no data.
     */
    public function renderEmptyText()
    {
        $emptyText=$this->emptyText===null ? Yii::t('zool','No results found.') : $this->emptyText;
        echo $emptyText;
    }

    /**
     * Renders the summary text.
     */
    public function renderSummary()
    {

        if(($count=$this->dataProvider->getItemCount())<=0)
            return;

        echo Xul::openTag('box', array(
                'class'=>$this->summaryCssClass));

        if($this->enablePagination)
        {
            $pagination=$this->dataProvider->getPagination();
            $total=$this->dataProvider->getTotalItemCount();
            $start=$pagination->currentPage*$pagination->pageSize+1;
            $end=$start+$count-1;
            if($end>$total)
            {
                $end=$total;
                $start=$end-$count+1;
            }
            if(($summaryText=$this->summaryText)===null)
                $summaryText=Yii::t('zool','Displaying {start}-{end} of 1 result.|Displaying {start}-{end} of {count} results.',$total);
            echo strtr($summaryText,array(
                    '{start}'=>$start,
                    '{end}'=>$end,
                    '{count}'=>$total,
                    '{page}'=>$pagination->currentPage+1,
                    '{pages}'=>$pagination->pageCount,
            ));
        }
        else
        {
            if(($summaryText=$this->summaryText)===null)
                $summaryText=Yii::t('zii','Total 1 result.|Total {count} results.',$count);
            echo strtr($summaryText,array(
                    '{count}'=>$count,
                    '{start}'=>1,
                    '{end}'=>$count,
                    '{page}'=>1,
                    '{pages}'=>1,
            ));
        }
        echo Xul::closeTag('box');
    }

    /**
     * Renders the pager.
     */
    public function renderPager()
    {
        if(!$this->enablePagination)
            return;

        $pager=array();
        $class='ZoolButtonPager';
        if(is_string($this->pager))
            $class=$this->pager;
        else if(is_array($this->pager))
        {
            $pager=$this->pager;
            if(isset($pager['class']))
            {
                $class=$pager['class'];
                unset($pager['class']);
            }
        }
        $pager['pages']=$this->dataProvider->getPagination();

        if($pager['pages']->getPageCount()>1)
        {
            echo Xul::box($this->widget($class, $pager, true), array('class'=> $this->pagerCssClass));
        }
        else
            $this->widget($class,$pager);
    }


}