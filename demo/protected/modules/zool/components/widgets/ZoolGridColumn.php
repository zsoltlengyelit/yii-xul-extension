<?php

Yii::import('zii.widgets.grid.CGridColumn');

/**
 *
 * @author Zsolt Lengyel
 *
 */
class ZoolGridColumn extends CGridColumn{

    /**
     * Renders the filter cell.
     * @since 1.1.1
     */
    public function renderFilterCell()
    {
        // TODO
//         echo CHtml::openTag('td',$this->filterHtmlOptions);
//         $this->renderFilterCellContent();
//         echo "</td>";
    }

    /**
     * Renders the header cell.
     */
    public function renderHeaderCell()
    {
        $this->headerHtmlOptions['id']=$this->id;

        $this->headerHtmlOptions['flex'] = 1;

        ob_start();
        $this->renderHeaderCellContent();

        $this->headerHtmlOptions['label'] = ob_get_clean();

        echo Xul::tag('treecol',$this->headerHtmlOptions, false, true)."\n";
    }

    /**
     * Renders a data cell.
     * @param integer $row the row number (zero-based)
     */
    public function renderDataCell($row)
    {
        $data=$this->grid->dataProvider->data[$row];
        $options=$this->htmlOptions;
        if($this->cssClassExpression!==null)
        {
            $class=$this->evaluateExpression($this->cssClassExpression,array('row'=>$row,'data'=>$data));
            if(!empty($class))
            {
                if(isset($options['class']))
                    $options['class'].=' '.$class;
                else
                    $options['class']=$class;
            }
        }

        ob_start();
        $this->renderDataCellContent($row,$data);

        $options['label'] = ob_get_clean();

        echo Xul::tag('treecell',$options)."\n";
    }

    /**
     * Renders the footer cell.
     */
    public function renderFooterCell()
    {
        // TODO
//         echo CHtml::openTag('td',$this->footerHtmlOptions);
//         $this->renderFooterCellContent();
//         echo '</td>';
    }


}