<?php

/**
 * 
 * @author Zsolti
 *
 */
class ZoolTabbox extends CWidget{
	
	/**
	 * 
	 * @var ZoolTab
	 */
	private $currentTab = null;
	
	/**
	 * Array of tabs.
	 * @var array[ZoolTab]
	 */
	public $tabs = array();
	
	/**
	 * Indexe from zero.
	 * @var integer
	 */
	public $selectedIndex = 0;
	
	public $tabboxOptions = array();
	
	public $tabsOptions = array();
	
	/**
	 * All the tabs will be ajaxRendered.
	 * @var boolean
	 */
	public $ajaxRendered = false;
	
	/**
	 * (non-PHPdoc)
	 * @see CWidget::init()
	 */
	public function init(){
		
		// set ID
		if(isset($this->tabboxOptions['id'])){		
			$this->setId($this->tabboxOptions['id']);
		}else{
			$this->tabboxOptions['id'] = $this->getId();
		}
		
		
		$this->tabboxOptions['selectedIndex'] = $this->selectedIndex;
		
		
		echo Xul::openTag('tabbox', $this->tabboxOptions);			
	}
	
	/**
	 * 
	 * @param string $label
	 * @param string $id
	 * @param array $options
	 */
	public function beginTab($label, $id, $options = array()){
		
		$options['label'] = $label;
		$options['id'] = $id;
		
		// children inherit the option
		if(!isset($options['ajaxRendered'])){
			$options['ajaxRendered'] = $this->ajaxRendered;
		}
		
		if(!isset($options['tabpanelId'])){
			$options['tabpanelId'] = $options['id'] . '-panel';
		}
		
		// the tab not ajaxrenderd, or required, or this is the active tab
		if(!$options['ajaxRendered'] 
				|| Xul::isRequired($options['tabpanelId'], true)
				|| count($this->tabs) === $this->selectedIndex){
				
			$this->currentTab = $this->tabs[$id] = $this->beginWidget('ZoolTab', $options, true);
			
			return true;
			
		}else{
			
			$this->currentTab = null;
			
			$this->tabs[$id] = $this->widget('ZoolTab', $options); // create empty tab
			
			return false;
		}
	}
	
	/**
	 * Ends tab if required.
	 */
	public function endTab(){
		if(null !== $this->currentTab)
			$this->endWidget();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see CWidget::run()
	 */	
	public function run(){
		
		$this->renderTabs();
		
		echo Xul::openTag('tabpanels');		

		foreach ($this->tabs as $tab){
			echo $tab->getContent();
		}
		
		echo Xul::closeTag('tabpanels');
		
		echo Xul::closeTag('tabbox');
		
	}
	
	/**
	 * 
	 */
	protected function renderTabs(){
		
		$this->tabsOptions['onselect'] = "this.selectedItem.oncommand();"; // trigger another event
		
		echo Xul::openTag('tabs', $this->tabsOptions);
			
		foreach ($this->tabs as $tabId => $tab){
				
			$tab->renderHeader();
				
		}
	
		echo Xul::closeTag('tabs');
	
	}
	
}