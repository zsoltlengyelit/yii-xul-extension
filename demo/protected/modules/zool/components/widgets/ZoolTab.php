<?php

/**
 * 
 * @author Zsolt Lengyel
 *
 */
class ZoolTab extends CWidget{
	
	/**
	 * Title of tab.
	 * @var string
	 */
	public $label = '';
		
	public $oncommand = null;
	
	public $xmlOptions = array();
	
	public $tabpanelId = null;
	
	/**
	 * The content will reRender after click on tab.
	 * @var boolean
	 */
	public $ajaxRendered = false;
	
	/**
	 * (non-PHPdoc)
	 * @see CWidget::init()
	 */
	public function init(){
		parent::init();
		
		if(!isset($this->xmlOptions['id'])){
			$this->xmlOptions['id'] = $this->getId();
		}
		
		if(null === $this->tabpanelId){
			$this->tabpanelId = $this->getId() .'-panel';
		}
		
		if($this->ajaxRendered == true){
			$this->oncommand = Xul::action(
					Yii::app()->request->requestUri,
					array('reRender'=>$this->tabpanelId));
		}
		
		if(null !== $this->oncommand){
			$this->xmlOptions['oncommand'] = $this->oncommand;
		}
		
		ob_start();
		
		echo Xul::openTag('tabpanel', array('id'=>$this->tabpanelId));	
	}
	
	/**
	 * (non-PHPdoc)
	 * @see CWidget::run()
	 */
	public function run(){
		
		echo Xul::closeTag('tabpanel');
		
		$this->content = ob_get_clean();
	}
	
	public function getContent(){
		return $this->content;
	}
	
	protected function setContent($content){
		$this->content = $content;
	}
	
	/**
	 * Renders tab part of panel. This means the header of panel.
	 */
	public function renderHeader(){
		
		$this->xmlOptions['label'] = $this->label;
		
		echo Xul::tag('tab', $this->xmlOptions);
	}
	
}