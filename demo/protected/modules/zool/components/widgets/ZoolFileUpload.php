<?php

/**
 * 
 * @author Zsolt Lengyel
 *
 */
class ZoolFileUpload extends CWidget{
	
	public $cssClass = "file-upload";
	
	public $textboxOptions = array();
	
	public $boxOptions = array();
	
	public $browseButtonOptions = array();
	
	public $browseLabel = "Browse";
	
	public $pickerJSFunctionPrefix = "Zool.uploadPicker";
	
	public $selectFileLabel = "Select a file";
	
	public $pickerFilter = 'filterAll';
	
	public $previewImage = true;
	
	/**
	 * Legal filters in XUL.
	 * @var array
	 */
	private $legalFilters = array('filterAll', 'filterHTML', 'filterText', 'filterImages', 'filterXML', 'filterXUL', 'filterApps', 'filterAllowURLs', 'filterAudio', 'filterVideo' );
	
	/**
	 * (non-PHPdoc)
	 * @see CWidget::init()
	 */
	public function init(){
		
		if(!in_array($this->pickerFilter, $this->legalFilters)){
			throw new ZoolException('Illegal filter for file picker: ' + $this->pickerFilter. ' Legals: ' + implode(', ', $this->legalFilters));
		}
		
		// register handle script
		$this->registerScript();
	}
	
	/**
	 * (non-PHPdoc)
	 * @see CWidget::run()
	 */
	public function run(){
		
		$this->boxOptions['id'] = $this->id;
		
		echo Xul::beginHbox($this->boxOptions);
		
		if($this->previewImage)
			echo Xul::image('', '', array(
				'id'=>$this->getPreviewImageId(),				
			));
		
		echo Xul::beginBox();
		
		$this->textboxOptions['readonly'] = 'true';
		$this->textboxOptions['id'] = $this->getTextboxId();
		$this->textboxOptions['class'] = $this->cssClass;
		
		echo Xul::tag('textbox', $this->textboxOptions);
		
		$this->browseButtonOptions['id'] = $this->id . '-browse-button';
		$this->browseButtonOptions['label'] = $this->browseLabel;
		
		$jsPreviewImage = $this->previewImage && $this->pickerFilter == 'filterImages' ? 'true' : 'false';
		
		$this->browseButtonOptions['oncommand'] = $this->pickerJSFunction . "(this, Zool.byId('{$this->getTextboxId()}'), $jsPreviewImage, Zool.byId('{$this->getPreviewImageId()}'));";
		
		echo Xul::tag('button', $this->browseButtonOptions);
		
		echo Xul::endBox();
		echo Xul::endHbox();
		
		
	}
	
	/**
	 * 
	 */
	protected function registerScript(){
		
		

		Yii::app()->clientScript->registerScript('zooluploadwidget-script', <<<SCRIPT
			
         {$this->getPickerJSFunction()} = function(button, textbox, previewImage, image) {

         	//netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");

         		var nsIFilePicker = Components.interfaces.nsIFilePicker;
           
         		var fp = Components.classes["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);
           
         		fp.init(window, "{$this->selectFileLabel}", nsIFilePicker.modeOpen);
           
         		fp.appendFilters( nsIFilePicker.{$this->pickerFilter} );

         		var res = fp.show();
           	
         		if (res == nsIFilePicker.returnOK) {
            		textbox.thefile = fp.file;	
             		textbox.value = textbox.thefile.path;
					
         			var filePath = textbox.thefile.path;
         				
			 		textbox.thefile = File(textbox.thefile.path);		
			 		
         			// trim path
         			var paths = textbox.value.split(DS);
			 		textbox.value = paths[paths.length -1];
         			
         			if(previewImage){

         				// 	preview file
	         			image.setAttribute('src', 'file:///' + filePath);
         				image.maxheight = 60;
         				image.maxwidth = 60;
         				image.width = 60;
	         			image.style = 'max-width: 60px, max-height: 60px;';
         			}
         			
          		} else {
             		textbox.thefile = null;
             		textbox.value = "";
           		}
         }
         	
SCRIPT
, CClientScript::POS_LOAD);
		
	}
	
	/**
	 * The generated ID of textbox.
	 * 
	 * @return string
	 */
	public function getTextboxId(){
		return $this->id . '-upload-box';
	}
	
	public function getPreviewImageId(){
		return $this->id . '-preview-image';
	}
	
	public function getPickerJSFunction(){
		return $this->pickerJSFunctionPrefix;
	}
	
}