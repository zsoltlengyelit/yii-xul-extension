<?php

Yii::import('system.web.widgets.pagers');

/**
 *
 * @author Zsolt Lengyel
 *
 */
class ZoolButtonPager extends CLinkPager{

    public $header = false;

    /**
     * Additional IDs to re-render.
     * @var string comma separated ids
     */
    public $reRender = '';

    /**
     * @var integer maximum number of page buttons that can be displayed. Defaults to 10.
     */
    public $maxButtonCount = 5;

    /**
     * Initializes the pager by setting some default property values.
     */
    public function init()
    {
        if($this->nextPageLabel===null)
            $this->nextPageLabel=Yii::t('zool','▶');
        if($this->prevPageLabel===null)
            $this->prevPageLabel=Yii::t('zool','◀');
        if($this->firstPageLabel===null)
            $this->firstPageLabel=Yii::t('zool','◀◀');
        if($this->lastPageLabel===null)
            $this->lastPageLabel=Yii::t('zool','▶▶');
        if($this->header===null)
            $this->header=Yii::t('zool','Go to page: ');

        if(!isset($this->htmlOptions['id']))
            $this->htmlOptions['id']=$this->getId();
        if(!isset($this->htmlOptions['class']))
            $this->htmlOptions['class']='yiiPager';

        if(!isset($this->htmlOptions['flex']))
            $this->htmlOptions['flex'] = 1;
    }

    /**
     * Executes the widget.
     * This overrides the parent implementation by displaying the generated page buttons.
     */
    public function run()
    {
        $buttons=$this->createPageButtons();
        if(empty($buttons))
            return;
        echo $this->header;
        echo Xul::tag('hbox',$this->htmlOptions,implode("\n",$buttons));
        echo $this->footer;
    }


    /**
     * Creates a page button.
     * You may override this method to customize the page buttons.
     * @param string $label the text label for the button
     * @param integer $page the page number
     * @param string $class the CSS class for the page button.
     * @param boolean $hidden whether this page button is visible
     * @param boolean $selected whether this page button is selected
     * @return string the generated button
     */
    protected function createPageButton($label,$page,$class,$hidden,$selected)
    {
        if($hidden || $selected)
            $class.=' '.($hidden ? $this->hiddenPageCssClass : $this->selectedPageCssClass);

        $options = array('class'=>$class, 'flex'=>1);

        $options['style'] = 'width: 30px; height: 20px;';
        if($selected){
            $options['style'] .= 'background-color: green; color: red;';
        }

        $reRender = $this->owner->id;
        if(!empty($this->reRender)){
            $reRender .= ', '. $this->reRender;
        }

        return Xul::linkButton($label, $this->createPageUrl($page), array('reRender'=> $reRender), $options);

    }


}