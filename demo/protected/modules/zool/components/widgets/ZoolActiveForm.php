<?php

/**
 *
 * @author Zsolt Lengyel
 *
 */
class ZoolActiveForm extends CActiveForm{

    /**
     * Name of function in JS submits the value of form
     *
     * @var string
     */
    public $submitJSFunction = 'App.submit';

    /**
     *
     * @var string rendered tag name
     */
    public $tagName = 'box';

    /**
     *
     */
    public $xmlOptions = array();

    /**
     *
     * @var boolean true if we want to submi the form on press enter key
     */
    public $onenterSubmit = true;

    /**
     *
     * @var string ID of DOM element which has automatic focus
     */
    public $autofocus = null;

    /**
     *
     * @var array JS submit configuration
     */
    public $submitConfig = array();

    /**
     * Strores the input ids
     * @var array
     */
    protected $inputIds = array();
    
    /**
     * Key: input id, value: input tag name
     * @var array
     */
    protected $inputTypes = array();

    /**
     *
     * @var string
     */
    protected $inputIdSeparator = ',';

    /**
     *
     * @var
     */
    protected $inputIdHolderIdSuffix = '-inputids';


    /**
	 * Initializes the widget.
	 * This renders the form open tag.
	 */
	public function init()
	{

	    if($this->htmlOptions != array()){
	        $this->xmlOptions = $this->htmlOptions;
	    }

		if(!isset($this->xmlOptions['id']))
			$this->xmlOptions['id']=$this->id;
		else
			$this->id=$this->xmlOptions['id'];

		if(false !== $this->tagName){
		    echo Xul::openTag($this->tagName, $this->xmlOptions);
		}
	}

	public function run(){

        $inputKeys = array();

        foreach ($this->inputIds as $id => $key){
            $inputKeys[] = "$id=$key";
        }

        $inputIds = implode($this->inputIdSeparator, $inputKeys);

        echo Xul::hiddenField($this->getInputIdsHolderId(), $inputIds);

        if(false !== $this->tagName){
	        echo Xul::closeTag($this->tagName);
        }

        $this->registerScripts();
	}
	
	public function registerSingleInput($id, $formName = null){
		
		if($formName === null){
			$formName = $id;
		}
		
		$this->inputIds[$id] = $formName;
	}

	/**
	 *
	 * @param object|string $model
	 * @param string $attribute
	 * @param array $htmlOptions
	 */
	public function registerInput($model, $attribute, &$htmlOptions){
			
	    // 1th param is input name, 2nd is Model[attribute] formula
	    if(is_string($model)){
	        $this->inputIds[$model] = $attributes;
	        return;
	    }


	    $key = get_class($model) . '[' . $attribute .']';

	    $id = Xul::resolveNameID($model, $attribute, $htmlOptions);

	    $this->inputIds[$id] = $key;	    
	}



    /**
     * Renders an HTML label for a model attribute.
     * This method is a wrapper of {@link CHtml::activeLabel}.
     * Please check {@link CHtml::activeLabel} for detailed information
     * about the parameters for this method.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $htmlOptions additional HTML attributes.
     * @return string the generated label tag
     */
    public function label($model,$attribute,$htmlOptions=array())
    {
        return Xul::activeLabel($model,$attribute,$htmlOptions);
    }

    /**
     * Renders an HTML label for a model attribute.
     * This method is a wrapper of {@link CHtml::activeLabelEx}.
     * Please check {@link CHtml::activeLabelEx} for detailed information
     * about the parameters for this method.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $htmlOptions additional HTML attributes.
     * @return string the generated label tag
     */
    public function labelEx($model,$attribute,$htmlOptions=array())
    {
        if(isset($htmlOptions['id'])){
            return Xul::label($model->getAttributeLabel($attribute), $htmlOptions['id']);
        }else
            return Xul::activeLabelEx($model,$attribute,$htmlOptions);
    }

    /**
     * Renders text row form model attribute.
     *
     * @param mixed $model
     * @param string $attribute
     * @param array $xmlOptions
     */
    public function textFieldRow($model, $attribute, $xmlOptions = array()){

        Xul::resolveNameID($model, $attribute, $xmlOptions);
        $o = $this->labelEx($model, $attribute, $xmlOptions['id']);
        $o .= $this->textField($model, $attribute, $xmlOptions);
        $o .= $this->error($model, $attribute);

        return $o;

    }
    
   
    /**
     * Renders password row form model attribute.
     *
     * @param mixed $model
     * @param string $attribute
     * @param array $xmlOptions
     */
    public function passwordFieldRow($model, $attribute, $xmlOptions = array()){

        Xul::resolveNameID($model, $attribute, $xmlOptions);
        $o = $this->labelEx($model, $attribute, $xmlOptions['id']);
        $o .= $this->passwordField($model, $attribute, $xmlOptions);
        $o .= $this->error($model, $attribute);

        return $o;

    }

    /**
     * Renders chackbox row form model attribute.
     *
     * @param mixed $model
     * @param string $attribute
     * @param array $xmlOptions
     */
    public function checkBoxRow($model, $attribute, $xmlOptions = array()){

        Xul::resolveNameID($model, $attribute, $xmlOptions);
        $o = $this->checkBox($model, $attribute, $xmlOptions);
        $o .= $this->error($model, $attribute);

        return $o;

    }
    
    /**
     * Renders a dropdown list for a model attribute.
     * This method is a wrapper of {@link CHtml::activeDropDownList}.
     * Please check {@link CHtml::activeDropDownList} for detailed information
     * about the parameters for this method.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $data data for generating the list options (value=>display)
     * @param array $htmlOptions additional HTML attributes.
     * @return string the generated drop down list
     */
    public function dropDownList($model,$attribute,$data,$xmlOptions=array())
    {
    	$this->registerInput($model, $attribute, $xmlOptions);
    	return Xul::activeDropDownList($model,$attribute,$data,$xmlOptions);
    }
    
    public function radiogroup($model, $attribute, $data, $xmlOptions = array()){
    	$this->registerInput($model, $attribute, $xmlOptions);
    	
    	return Xul::activeRadiogroup($model,$attribute,$data,$xmlOptions);
    }
    
    
    /**
     * Renders a checkbox for a model attribute.
     * This method is a wrapper of {@link CHtml::activeCheckBox}.
     * Please check {@link CHtml::activeCheckBox} for detailed information
     * about the parameters for this method.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $htmlOptions additional HTML attributes.
     * @return string the generated check box
     */
    public function checkBox($model,$attribute,$xmlOptions=array())
    {
    	$this->registerInput($model, $attribute, $xmlOptions);
    	return Xul::activeCheckBox($model,$attribute,$xmlOptions);
    }

    /**
     * Renders a text field for a model attribute.
     * This method is a wrapper of {@link CHtml::activeTextField}.
     * Please check {@link CHtml::activeTextField} for detailed information
     * about the parameters for this method.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $htmlOptions additional HTML attributes.
     * @return string the generated input field
     */
    public function textField($model,$attribute,$xmlOptions=array())
    {
        $this->registerInput($model, $attribute, $xmlOptions);
        return Xul::activeTextbox($model,$attribute,$xmlOptions);
    }
    
    /**
     * Renders a number field for a model attribute.
     * This method is a wrapper of {@link CHtml::activeNumberField}.
     * Please check {@link CHtml::activeNumberField} for detailed information
     * about the parameters for this method.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $htmlOptions additional HTML attributes.
     * @return string the generated input field
     * @since 1.1.11
     */
    public function numberField($model,$attribute,$xmlOptions=array())
    {
    	$this->registerInput($model, $attribute, $xmlOptions);
    	return Xul::activeNumberField($model,$attribute,$xmlOptions);
    }
    

    /**
     * Renders a hidden field for a model attribute.
     * This method is a wrapper of {@link CHtml::activeHiddenField}.
     * Please check {@link CHtml::activeHiddenField} for detailed information
     * about the parameters for this method.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $htmlOptions additional HTML attributes.
     * @return string the generated input field
     */
    public function hiddenField($model,$attribute,$xmlOptions=array())
    {
        $this->registerInput($model, $attribute, $xmlOptions);
        return Xul::activeHiddenField($model,$attribute,$xmlOptions);
    }

    /**
     * Renders a password field for a model attribute.
     * This method is a wrapper of {@link CHtml::activePasswordField}.
     * Please check {@link CHtml::activePasswordField} for detailed information
     * about the parameters for this method.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $htmlOptions additional HTML attributes.
     * @return string the generated input field
     */
    public function passwordField($model,$attribute,$xmlOptions=array())
    {
        $this->registerInput($model, $attribute, $xmlOptions);
        return Xul::activePasswordField($model,$attribute,$xmlOptions);
    }

    /**
     * Renders a text area for a model attribute.
     * This method is a wrapper of {@link CHtml::activeTextArea}.
     * Please check {@link CHtml::activeTextArea} for detailed information
     * about the parameters for this method.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $htmlOptions additional HTML attributes.
     * @return string the generated text area
     */
    public function textArea($model,$attribute,$xmlOptions=array())
    {
        $this->registerInput($model, $attribute, $xmlOptions);
        return Xul::activeTextArea($model,$attribute,$xmlOptions);
    }
  

    /**
     * Displays the first validation error for a model attribute.
     * This is similar to {@link CHtml::error} except that it registers the model attribute
     * so that if its value is changed by users, an AJAX validation may be triggered.
     * @param CModel $model the data model
     * @param string $attribute the attribute name
     * @param array $htmlOptions additional HTML attributes to be rendered in the container div tag.
     * Besides all those options available in {@link CHtml::error}, the following options are recognized in addition:
     * <ul>
     * <li>validationDelay</li>
     * <li>validateOnChange</li>
     * <li>validateOnType</li>
     * <li>hideErrorMessage</li>
     * <li>inputContainer</li>
     * <li>errorCssClass</li>
     * <li>successCssClass</li>
     * <li>validatingCssClass</li>
     * <li>beforeValidateAttribute</li>
     * <li>afterValidateAttribute</li>
     * </ul>
     * These options override the corresponding options as declared in {@link options} for this
     * particular model attribute. For more details about these options, please refer to {@link clientOptions}.
     * Note that these options are only used when {@link enableAjaxValidation} or {@link enableClientValidation}
     * is set true.
     *
     * When client-side validation is enabled, an option named "clientValidation" is also recognized.
     * This option should take a piece of JavaScript code to perform client-side validation. In the code,
     * the variables are predefined:
     * <ul>
     * <li>value: the current input value associated with this attribute.</li>
     * <li>messages: an array that may be appended with new error messages for the attribute.</li>
     * <li>attribute: a data structure keeping all client-side options for the attribute</li>
     * </ul>
     * @param boolean $enableAjaxValidation whether to enable AJAX validation for the specified attribute.
     * Note that in order to enable AJAX validation, both {@link enableAjaxValidation} and this parameter
     * must be true.
     * @param boolean $enableClientValidation whether to enable client-side validation for the specified attribute.
     * Note that in order to enable client-side validation, both {@link enableClientValidation} and this parameter
     * must be true. This parameter has been available since version 1.1.7.
     * @return string the validation result (error display or success message).
     * @see CHtml::error
     */
    public function error($model,$attribute,$htmlOptions=array(),$enableAjaxValidation=true,$enableClientValidation=true)
    {
        if(!$this->enableAjaxValidation)
            $enableAjaxValidation=false;
        if(!$this->enableClientValidation)
            $enableClientValidation=false;

        if(!isset($htmlOptions['class']))
            $htmlOptions['class']=$this->errorMessageCssClass;

        if(!$enableAjaxValidation && !$enableClientValidation)
            return CHtml::error($model,$attribute,$htmlOptions);

        $id=CHtml::activeId($model,$attribute);
        $inputID=isset($htmlOptions['inputID']) ? $htmlOptions['inputID'] : $id;
        unset($htmlOptions['inputID']);
        if(!isset($htmlOptions['id']))
            $htmlOptions['id']=$inputID.'_em_';

        $option=array(
                'id'=>$id,
                'inputID'=>$inputID,
                'errorID'=>$htmlOptions['id'],
                'model'=>get_class($model),
                'name'=>$attribute,
                'enableAjaxValidation'=>$enableAjaxValidation,
        );

        $optionNames=array(
                'validationDelay',
                'validateOnChange',
                'validateOnType',
                'hideErrorMessage',
                'inputContainer',
                'errorCssClass',
                'successCssClass',
                'validatingCssClass',
                'beforeValidateAttribute',
                'afterValidateAttribute',
        );
        foreach($optionNames as $name)
        {
            if(isset($htmlOptions[$name]))
            {
                $option[$name]=$htmlOptions[$name];
                unset($htmlOptions[$name]);
            }
        }
        if($model instanceof CActiveRecord && !$model->isNewRecord)
            $option['status']=1;

        if($enableClientValidation)
        {
            $validators=isset($htmlOptions['clientValidation']) ? array($htmlOptions['clientValidation']) : array();

            $attributeName = $attribute;
            if(($pos=strrpos($attribute,']'))!==false && $pos!==strlen($attribute)-1) // e.g. [a]name
            {
                $attributeName=substr($attribute,$pos+1);
            }

            foreach($model->getValidators($attributeName) as $validator)
            {
                if($validator->enableClientValidation)
                {
                    if(($js=$validator->clientValidateAttribute($model,$attributeName))!='')
                        $validators[]=$js;
                }
            }
            if($validators!==array())
                $option['clientValidation']=new CJavaScriptExpression("function(value, messages, attribute) {\n".implode("\n",$validators)."\n}");
        }

        $html=Xul::error($model,$attribute,$htmlOptions);
        if($html==='')
        {
            if(isset($htmlOptions['style']))
                $htmlOptions['style']=rtrim($htmlOptions['style'],';').';display:none';
            else
                $htmlOptions['style']='display:none';
            $html=Xul::tag('box',$htmlOptions,'');
        }

        $this->attributes[$inputID]=$option;
        return $html;
    }


    /**
     *
     * @param string $label
     * @param array $config
     * @param array $xmlOptions
     */
    public function submit($label = 'OK', $config = array(), $xmlOptions = array()){

        return Xul::button($label, $this->getSubmitScript($config), $xmlOptions);

    }

    /**
     * Javascript call, which submits the form.
     * @return string JS function
     *
     */
    public function getSubmitScript($config = array()){

        if(empty($config))
            $config = $this->submitConfig;

        $config['inputIdList'] = "Zool.byId(\"{$this->getInputIdsHolderId()}\").value";
        $config['inputIdSeparator'] = $this->inputIdSeparator;
        $config['method'] = $this->method;

        $configJSON = ZoolJSON::encode($config, true);
        
        $link = $this->getActionUrl();
        
        // append reRender
        if(strpos($link, 'reRender=') === false && isset($config['reRender'])){
        	 
        	$link = trim($link, '&');
        	 
        	if(strpos($link, '?') === false){
        		$link .= '?reRender='.$config['reRender'];
        	}else{
        		$link .= '&reRender='.$config['reRender'];
        	}
        	 
        }

        return $this->submitJSFunction . "(". "'$link'," . $configJSON  . ")";
    }

    protected function getActionUrl(){

        if(strpos($this->action, 'http') === 0){ // this is an absolute URL
            return $this->action;
        }

        return Yii::app()->controller->createAbsoluteUrl($this->action);
    }

    /**
     * @return id of holder of input ids
     */
    protected function getInputIdsHolderId(){
        return $this->id . $this->inputIdHolderIdSuffix;
    }

    /**
     * Registers javascripts.
     */
    protected function registerScripts(){

        // register autofocus
        if($this->autofocus != null){

            $script = 'Zool.byId("' . $this->autofocus .'").focus();';

            Yii::app()->clientScript->registerScript($this->id . '-autofocus', $script , CClientScript::POS_LOAD);

        }

        // register on enter submit script
        if($this->onenterSubmit){

            $listener = "var enterListener = function(event){
                if(event.keyCode == KeyEvent.DOM_VK_ENTER || event.keyCode == KeyEvent.DOM_VK_RETURN) {
                    {$this->getSubmitScript()};
                    event.stopPropagation();
                }
             }";

            $listenerScripts = array();
            foreach ($this->inputIds as $inputId => $modelAttribute){

                $listenerScripts[] = 'Zool.byId("' . $inputId . '").onkeypress = enterListener;';

            }

            Yii::app()->clientScript->registerScript($this->id . '-entersubmitscripts', $listener. "\n" . implode("\n", $listenerScripts), CClientScript::POS_LOAD);

        }

    }

}