<?php

/**
 *
 * @author Zsolt Lengyel
 *
 */
class ZoolHttpRequest extends CHttpRequest{

    /**
     * Returns whether this is an AJAX (XMLHttpRequest) request.
     * @return boolean whether this is an AJAX (XMLHttpRequest) request.
     */
    public function getIsAjaxRequest()
    {
        // TODO kidolgozni erre egy módszert
        return true;
        //return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH']==='XMLHttpRequest';
    }

}