<?php

/**
 * To get pure, unparsed output, add the 'zool_pure_output' parameter for your  request.
 *
 * @author Zsolt Lengyel
 *
 */
class ZoolController extends CController{


    private $actionOutputs = array();
    
    private $prefixes = array();
    
    /**
     * 
     * @param string $key
     * @param mixed $value
     * @throws ZoolException when key is not string
     */
    public function setPrefix($key, $value){
    	if(!is_string($key)){
    		throw new ZoolException('Prefix key must be string.');
    	}
    	$this->prefixes[$key] = $value;
    } 

    /**
     *
     * @return array name of actions, where the XUL parsing must execute
    */
    public function zoolActions(){
        return array();
    }

    public function beforeAction($action){

        parent::beforeAction($action);
        
        // tracing the request
        Yii::log(print_r($_REQUEST, true), CLogger::LEVEL_TRACE, 'zool.request');
        Yii::log(print_r($_POST, true), CLogger::LEVEL_TRACE, 'zool.post');
        Yii::log(print_r($_GET, true), CLogger::LEVEL_TRACE, 'zool.get');
        Yii::log(print_r($_FILES, true), CLogger::LEVEL_TRACE, 'zool.files');

        ob_start();

        return true;

    }


    /**
     * (non-PHPdoc)
     * @see CController::afterAction()
     */
    public function afterAction($action){

        parent::afterAction($action);
        
        // end of processing
        Yii::app()->onEndRequest(new CEvent(Yii::app()));

        // get the collected output
        $output = ob_get_clean();

        $zool = Yii::app()->getModule('zool');
        $zoolActions = $this->zoolActions();

        $excepteds = isset($zoolActions['except']) ? explode(',', $zoolActions['except']) : array();
        
        $excepteds  = array_map('trim', $excepteds);
         
        $everyActionIsZoolAction = (is_string($zoolActions) && '*' == $zoolActions) || (is_array($zoolActions) && !empty($zoolActions) && $zoolActions[0] == '*');

        if(!in_array($action, $excepteds) && ($everyActionIsZoolAction ||  in_array($this->getAction()->getId(), $zoolActions))) {

            // debug option
            $pureOutput = YII_DEBUG && isset($_REQUEST['zool_pure_output']) && $_REQUEST['zool_pure_output'];

            if($zool->outputFormat == 'json' && !$pureOutput){

                ZoolModule::import('util.XmlParser');
                ZoolModule::import('util.ZoolJSON');
              
                // prefixes has lower
                $prefix = CMap::mergeArray(Yii::app()->getModule('zool')->getPrefixes(), Yii::app()->clientScript->lastScripts);
                
                $output = array(
                        'prefix'=> $prefix,

                        'content' => XmlParser::toTree($output),

                        'messages' => Yii::app()->user->getFlashes(),
                );

                // encode to json string
                $output = ZoolJSON::encode($output);

                header('Content-Type: application/json');

                echo $output;

               	return;
            }
        }

        echo $output;
       
    }

}