<?php

/**
 * Simple log for Zool client.
 * 
 * @author Zsolt Lengyel
 *
 */
class ZoolLogRoute extends CLogRoute{
	
	public function init(){		
		parent::init();
		
		// register log handler
		Yii::app()->clientScript->registerScript('Zool.prefixHandlers.log', <<<SCRIPT
		Zool.prefixHandlers.log = function(logs){
				for(key in logs)
					Sys.services.console().logStringMessage('Server log: ' + logs[key]);
		};
SCRIPT
, CClientScript::POS_BEGIN);
		
	}
	
	/**
	 * Processes log messages and sends them to specific destination.
	 * Derived child classes must implement this method.
	 * @param array $logs list of messages. Each array element represents one message
	 * with the following structure:
	 * array(
	 *   [0] => message (string)
	 *   [1] => level (string)
	 *   [2] => category (string)
	 *   [3] => timestamp (float, obtained by microtime(true));
	 */
	protected function processLogs($logs){		
		$zoolLogs = array();
		
		foreach ($logs as $log){
			$zoolLogs[] = $this->formatLogMessage($log[0], $log[1], $log[2], $log[3]);
		}	
		
		Yii::app()->getModule('zool')->setPrefix('log', $zoolLogs);
	}
	
}