<?php

/**
 * Similar to self.
 *
 * @author Zsolt Lengyel
 *
 */
class Xul extends CHtml{
	
	/**
	 * JS script to stop event propagation to parent.
	 * @var string
	 */
	const EVENT_STOP_PROPAGATION = "event.stopPropagation();";
	
	const CONFIRM_SCRIPT = "Zool.confirm";

    public static $afterRequiredLabel=' *';

    private static $idCounter = 0;

    /**
     *
     * @return string unique ID
     */
    public static function uniqueId(){
        return 'xulid_' . self::$idCounter++;
    }
    
    public static function cdata($text)
    {
    	return "<![CDATA[\n" . $text . "\n]]>";
    }


    /**
     *
     * @param unknown_type $title
     * @param unknown_type $options
     * @param unknown_type $content
     * @return string
     */
    public static function window($title = ' ', $options = array(), $content = false){
        return self::tag('window', $options, $content);
    }

    public static function box($content = '', $options = array()){
        return self::tag('box', $options, $content);
    }

    public static function hbox($content = ' ', $options = array()){
        return self::tag('hbox', $options, $content);
    }

    public static function vbox($content = ' ', $options = array()){
        return self::tag('vbox', $options, $content);
    }

    public static function beginBox($options = array()){
        return self::tag('box', $options, false, false);
    }

    public static function endBox(){
        return '</box>';
    }

    public static function beginHbox($options = array()){
        return self::tag('hbox', $options, false, false);
    }

    public static function endHbox(){
        return '</hbox>';
    }

    public static function beginVbox($options = array()){
        return self::tag('vbox', $options, false, false);
    }

    public static function endVbox(){
        return '</vbox>';
    }
    
    public static function beginPanel($id, $options = array()){
    	$options['id'] = $id;
    	return self::tag('panel', $options, false, false);
    }
    
    public static function endPanel(){
    	return '</panel>';
    }
    
    public static function spacer($flex = -1, $options = array()){
    	if($flex != -1){
    		$options['flex'] = $flex;
    	}
    	
    	return self::tag('spacer', $options);
    }
    
    public static function description($value, $options = array()){
    	$options['value'] = $value;
    	return self::tag('description', $options);
    }
    
    public static function beginGroupbox($id = null, $flex = null, $caption = null, $options =array()){
    	
    	if($id !== null)
    		$options['id'] = $id;
    	if($flex !== null)
    		$options['flex'] = $flex;
    	
    	$o = self::openTag('groupbox', $options);
    	
    	if($caption !== null){
    		$o .= self::tag('caption', array('label'=>$caption));
    	}
    	
    	return $o;
    }
    
    public static function endGroupbox(){
    	return self::closeTag('groupbox');
    }

    /**
     * Generates an image tag.
     * @param string $src the image URL
     * @param string $alt the alternative text display
     * @param array $htmlOptions additional HTML attributes (see {@link tag}).
     * @return string the generated image tag
     */
    public static function image($src,$alt='',$htmlOptions=array())
    {
        $htmlOptions['src']=$src;
        $htmlOptions['alt']=$alt;
        return self::tag('image',$htmlOptions);
    }
	
    public static function command($id, $oncommand){
    	
    	return Xul::tag('command', array(
    			'id'=>$id,
    			'oncommand'=>$oncommand
    			));
    	
    }


    /**
     * Generates a label tag.
     * @param string $label label text. Note, you should HTML-encode the text if needed.
     * @param string $for the ID of the HTML element that this label is associated with.
     * If this is false, the 'for' attribute for the label tag will not be rendered.
     * @param array $htmlOptions additional HTML attributes.
     * The following HTML option is recognized:
     * <ul>
     * <li>required: if this is set and is true, the label will be styled
     * with CSS class 'required' (customizable with self::$requiredCss),
     * and be decorated with {@link self::beforeRequiredLabel} and
     * {@link self::afterRequiredLabel}.</li>
     * </ul>
     * @return string the generated label tag
     */
    public static function label($label,$for,$htmlOptions=array())
    {
        if($for===false)
            unset($htmlOptions['control']);
        else
            $htmlOptions['control']=$for;
        if(isset($htmlOptions['required']))
        {
            if($htmlOptions['required'])
            {
                if(isset($htmlOptions['class']))
                    $htmlOptions['class'].=' '.self::$requiredCss;
                else
                    $htmlOptions['class']=self::$requiredCss;
                $label=self::$beforeRequiredLabel.$label.self::$afterRequiredLabel;
            }
            unset($htmlOptions['required']);
        }
        $htmlOptions['value'] = $label;

        return self::tag('label',$htmlOptions);
    }

    /**
     * Makes general action link.
     *
     * @param string|array $url Yii action
     * @param array $config JS configuration
     * @return string Zooll call method
     */
    public static function action($url, $config = array()){

        $link = self::normalizeUrl($url);
        $jsonconfig = ZoolJSON::encode($config);
        
        // append rerender
        if(strpos($link, 'reRender=') === false && isset($config['reRender'])){
        	
        	$link = trim($link, '&');
        	
        	if(strpos($link, '?') === false){
        		$link .= '?reRender='.$config['reRender'];
        	}else{
        		$link .= '&reRender='.$config['reRender'];
        	}
        	
        }
        
        $script = "App.link('$link', $jsonconfig)";
        
        return self::wrapConfirm($script, $config);
    }
    
    /**
     * 
     * @param srting $js the JS to confirm
     * @param array $options key: confirm, boolean to indicate confirm. 
     * confirmTitle,
     * confirmContent,
     * confirmOtherwise
     * 
     * @return string the wrapped string
     */
    public static function wrapConfirm($js, $options = array()){
    	
    	if(isset($options['confirm']) && $options['confirm']){
    		
    		$title = isset($options['confirmTitle']) ? $options['confirmTitle'] : Yii::t('zool', 'Confirmation');
    		$content = isset($options['confirmContent']) ? $options['confirmContent'] : Yii::t('zool', 'Are you sure?');
    		$otherwise = isset($options['confirmOtherwise']) ? $options['confirmOtherwise'] : '';
    		
    		return "if(". self::CONFIRM_SCRIPT ."('$title', '$content')){ ".$js."; }else{ ".$otherwise."; }";    		
    	}
    	
    	// no confirmation
    	return $js;
    	
    }

    /**
     *
     * @param string $label label
     * @param array,string $url Yii URL
     * @param array $config JS configuration
     * @param array $options XML options
     * @return string rendered tag
     */
    public static function linkButton($label, $url, $config = array(), $options = array()){
        $command = self::action($url, $config);
        return self::button($label, $command, $options);
    }
    
    /**
     *
     * @param string $label label
     * @param array,string $url Yii URL
     * @param array $config JS configuration
     * @param array $options XML options
     * @return string rendered tag
     */
    public static function toolbarbutton($label, $url, $config = array(), $options = array()){
    	$command = self::action($url, $config);
    	
    	$options['oncommand'] = $command;
    	$options['label'] = $label;
    	
    	return self::tag('toolbarbutton', $options);
    }
    
    /**
     *
     * @param string $label label
     * @param array,string $url Yii URL
     * @param array $config JS configuration
     * @param array $options XML options
     * @return string rendered tag
     */
    public static function beginToolbarbutton($label, $url, $config = array(), $options = array()){
    	$command = self::action($url, $config);
    	
    	$options['oncommand'] = $command;
    	$options['label'] = $label;
    	
    	return self::openTag('toolbarbutton', $options);
    }
    
    public static function endToolbarbutton(){
    	return self::closeTag('toolbarbutton');
    }
    
    /**
     *
     * @param string $label label
     * @param array,string $url Yii URL
     * @param array $config JS configuration
     * @param array $options XML options
     * @return string rendered tag
     */
    public static function beginLinkButton($label, $url, $config = array(), $options = array()){
    
    	$command = self::action($url, $config);
    
    	return self::beginButton($label, $command, $options);
    
    }
    
    public static function endLinkButton(){
    	return self::endButton();	
    }
    
    public static function button($label, $oncommand = '', $options = array()){

        $options['oncommand'] = $oncommand;
        $options['label'] = $label;

        return self::tag('button', $options);
    }

    public static function beginButton($label, $oncommand = '', $options = array()){
    
    	$options['oncommand'] = $oncommand;
    	$options['label'] = $label;
    
    	return self::openTag('button', $options);
    }
    
    public static function endButton(){
    	return self::closeTag('button');
    }
    
    public static function beginMenupopup(){
    	return self::openTag('menupopup');
    }
    
    public static function endMenupopup(){
    	return self::closeTag('menupopup');
    }
    
    public static function menuitem($label, $oncommand = null, $options = array(), $stopPropagation = false){
    	
    	$options['label'] = $label;
    	
    	if(null !== $oncommand)
    		$options['oncommand'] = $oncommand;
    	
    	if($stopPropagation){
    		
    		if(isset($options['oncommand'])){
    			$options['oncommand'] .= ';' . self::EVENT_STOP_PROPAGATION;
    		}else{
    			$options['oncommand'] = self::EVENT_STOP_PROPAGATION;
    		}
    		
    	}
    	
    	return self::tag('menuitem', $options);
    }
    
    public static function textbox($id, $value = '', $xmlOptions = array()){
        $xmlOptions['id'] = $id;
        $xmlOptions['value'] = $value;

        return self::tag('textbox', $xmlOptions);
    }

    public static function passwordBox($id, $value = '', $xmlOptions = array()){
        $xmlOptions['type'] = 'password';
        return self::textbox($id, $value, $xmlOptions);
    }

    public static function searchBox($id, $value = '', $xmlOptions = array()){
        $xmlOptions['type'] = 'search';
        return self::textbox($id, $value, $xmlOptions);
    }

    public static function numberBox($id, $value = '', $xmlOptions = array()){
        $xmlOptions['type'] = 'number';
        return self::textbox($id, $value, $xmlOptions);
    }

    public static function autocompleteBox($id, $value = '', $xmlOptions = array()){
        $xmlOptions['type'] = 'autocomplete';
        return self::textbox($id, $value, $xmlOptions);
    }

    /**
     * Generates a hidden input.
     * @param string $name the input name
     * @param string $value the input value
     * @param array $htmlOptions additional HTML attributes (see {@link tag}).
     * @return string the generated input field
     * @see inputField
     */
    public static function hiddenField($id, $value='',$htmlOptions=array())
    {
        $htmlOptions['style'] = 'display:none;';
        return self::textbox($id, $value, $htmlOptions);
    }
    
    /**
     * 
     * @param string $label
     * @param string $value
     * @param array $xmlOptions
     */
    public static function radio($label, $value = null, $xmlOptions = array()){
    	$xmlOptions['label'] = $label;
    	if(null !== $value){
    		$xmlOptions['value'] = $value;
    	}
    	
    	return self::tag('radio', $xmlOptions);
    }
    
    public static function radiogroup($id, $value = null, $data = array(), $xmlOptions = array(), $radioOptions = array()){
    	
    	$xmlOptions['id'] = $id;
    	   	
    	$o = self::openTag('radiogroup', $xmlOptions);
    	
    	foreach ($data as $itemvalue => $label){
    		$options = $radioOptions;
    		
    		// set selected
    		if($value !== null && $itemvalue == $value)
    			$options['selected'] = 'true';
    		
    		$o .= self::radio($label, $itemvalue, $options);
    	}
    	
    	$o .= self::closeTag('radiogroup');
    	
    	return $o;
    }
    
    public static function listitem($label, $value = null, $selected = false, $options = array()){
    	$options['label'] = $label;
    	if(null !== $value){
    		$options['value'] = $value;
    	}
    	$options['selected'] = $selected ? 'true' : 'false';
    	
    	return self::tag('listitem', $options);
    }

    /**
     * Generates a label tag for a model attribute.
     * The label text is the attribute label and the label is associated with
     * the input for the attribute (see {@link CModel::getAttributeLabel}.
     * If the attribute has input error, the label's CSS class will be appended with {@link errorCss}.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $htmlOptions additional HTML attributes. The following special options are recognized:
     * <ul>
     * <li>required: if this is set and is true, the label will be styled
     * with CSS class 'required' (customizable with self::$requiredCss),
     * and be decorated with {@link self::beforeRequiredLabel} and
     * {@link self::afterRequiredLabel}.</li>
     * <li>label: this specifies the label to be displayed. If this is not set,
     * {@link CModel::getAttributeLabel} will be called to get the label for display.
     * If the label is specified as false, no label will be rendered.</li>
     * </ul>
     * @return string the generated label tag
     */
    public static function activeLabel($model,$attribute,$htmlOptions=array())
    {
        if(isset($htmlOptions['for']))
        {
            $for=$htmlOptions['for'];
            unset($htmlOptions['for']);
        }
        else
            $for=self::getIdByName(self::resolveName($model,$attribute));
        if(isset($htmlOptions['label']))
        {
            if(($label=$htmlOptions['label'])===false)
                return '';
            unset($htmlOptions['label']);
        }
        else
            $label=$model->getAttributeLabel($attribute);
        if($model->hasErrors($attribute))
            self::addErrorCss($htmlOptions);
        return self::label($label,$for,$htmlOptions);
    }

    /**
     * Generates a label tag for a model attribute.
     * This is an enhanced version of {@link activeLabel}. It will render additional
     * CSS class and mark when the attribute is required.
     * In particular, it calls {@link CModel::isAttributeRequired} to determine
     * if the attribute is required.
     * If so, it will add a CSS class {@link self::requiredCss} to the label,
     * and decorate the label with {@link self::beforeRequiredLabel} and
     * {@link self::afterRequiredLabel}.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $htmlOptions additional HTML attributes.
     * @return string the generated label tag
     */
    public static function activeLabelEx($model,$attribute,$htmlOptions=array())
    {
        $realAttribute=$attribute;
        self::resolveName($model,$attribute); // strip off square brackets if any
        $htmlOptions['required']=$model->isAttributeRequired($attribute);
        return self::activeLabel($model,$realAttribute,$htmlOptions);
    }

    /**
     * Generates a text field input for a model attribute.
     * If the attribute has input error, the input field's CSS class will
     * be appended with {@link errorCss}.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $htmlOptions additional HTML attributes. Besides normal HTML attributes, a few special
     * attributes are also recognized (see {@link clientChange} and {@link tag} for more details.)
     * @return string the generated input field
     * @see clientChange
     * @see activeInputField
     */
    public static function activeTextbox($model,$attribute,$htmlOptions=array())
    {
        self::resolveNameID($model,$attribute,$htmlOptions);
        self::clientChange('change',$htmlOptions);
        return self::activeInputField('text',$model,$attribute,$htmlOptions);
    }

    /**
     * Generates a url field input for a model attribute.
     * If the attribute has input error, the input field's CSS class will
     * be appended with {@link errorCss}.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $htmlOptions additional HTML attributes. Besides normal HTML attributes, a few special
     * attributes are also recognized (see {@link clientChange} and {@link tag} for more details.)
     * @return string the generated input field
     * @see clientChange
     * @see activeInputField
     * @since 1.1.11
     */
    public static function activeUrlField($model,$attribute,$htmlOptions=array())
    {
        self::resolveNameID($model,$attribute,$htmlOptions);
        //self::clientChange('change',$htmlOptions);
        // TODO
        return self::activeInputField('url',$model,$attribute,$htmlOptions);
    }

    /**
     * Generates an email field input for a model attribute.
     * If the attribute has input error, the input field's CSS class will
     * be appended with {@link errorCss}.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $htmlOptions additional HTML attributes. Besides normal HTML attributes, a few special
     * attributes are also recognized (see {@link clientChange} and {@link tag} for more details.)
     * @return string the generated input field
     * @see clientChange
     * @see activeInputField
     * @since 1.1.11
     */
    public static function activeEmailField($model,$attribute,$htmlOptions=array())
    {
        self::resolveNameID($model,$attribute,$htmlOptions);
        self::clientChange('change',$htmlOptions);
        return self::activeInputField('email',$model,$attribute,$htmlOptions);
    }

    /**
     * Generates a number field input for a model attribute.
     * If the attribute has input error, the input field's CSS class will
     * be appended with {@link errorCss}.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $htmlOptions additional HTML attributes. Besides normal HTML attributes, a few special
     * attributes are also recognized (see {@link clientChange} and {@link tag} for more details.)
     * @return string the generated input field
     * @see clientChange
     * @see activeInputField
     * @since 1.1.11
     */
    public static function activeNumberField($model,$attribute,$htmlOptions=array())
    {
        self::resolveNameID($model,$attribute,$htmlOptions);        
        return self::activeInputField('number',$model,$attribute,$htmlOptions);
    }

    /**
     * Generates a range field input for a model attribute.
     * If the attribute has input error, the input field's CSS class will
     * be appended with {@link errorCss}.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $htmlOptions additional HTML attributes. Besides normal HTML attributes, a few special
     * attributes are also recognized (see {@link clientChange} and {@link tag} for more details.)
     * @return string the generated input field
     * @see clientChange
     * @see activeInputField
     * @since 1.1.11
     */
    public static function activeRangeField($model,$attribute,$htmlOptions=array())
    {
        self::resolveNameID($model,$attribute,$htmlOptions);
        // self::clientChange('change',$htmlOptions);
        // TODO
        return self::activeInputField('range',$model,$attribute,$htmlOptions);
    }


    public static function activeDatepicker($model,$attribute,$htmlOptions=array())
    {
        self::resolveNameID($model,$attribute,$htmlOptions);
        // self::clientChange('change',$htmlOptions);
        // TODO
        if($model->hasErrors($attribute))
            self::addErrorCss($htmlOptions);
        $text=self::resolveValue($model,$attribute);
        $htmlOptions['value'] = $text;
        return self::tag('datepicker',$htmlOptions);
    }
    
    /**
     * Generates a drop down list for a model attribute.
     * If the attribute has input error, the input field's CSS class will
     * be appended with {@link errorCss}.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $data data for generating the list options (value=>display)
     * You may use {@link listData} to generate this data.
     * Please refer to {@link listOptions} on how this data is used to generate the list options.
     * Note, the values and labels will be automatically HTML-encoded by this method.
     * @param array $htmlOptions additional HTML attributes. Besides normal HTML attributes, a few special
     * attributes are recognized. See {@link clientChange} and {@link tag} for more details.
     * In addition, the following options are also supported:
     * <ul>
     * <li>encode: boolean, specifies whether to encode the values. Defaults to true.</li>
     * <li>prompt: string, specifies the prompt text shown as the first list option. Its value is empty.  Note, the prompt text will NOT be HTML-encoded.</li>
     * <li>empty: string, specifies the text corresponding to empty selection. Its value is empty.
     * The 'empty' option can also be an array of value-label pairs.
     * Each pair will be used to render a list option at the beginning. Note, the text label will NOT be HTML-encoded.</li>
     * <li>options: array, specifies additional attributes for each OPTION tag.
     *     The array keys must be the option values, and the array values are the extra
     *     OPTION tag attributes in the name-value pairs. For example,
     * <pre>
     *     array(
     *         'value1'=>array('disabled'=>true, 'label'=>'value 1'),
     *         'value2'=>array('label'=>'value 2'),
     *     );
     * </pre>
     * </li>
     * </ul>
     * @return string the generated drop down list
     * @see clientChange
     * @see listData
     */
    public static function activeDropDownList($model,$attribute,$data,$htmlOptions=array())
    {
    	self::resolveNameID($model,$attribute,$htmlOptions);
    	$selection=self::resolveValue($model,$attribute);
    	
    	$options = self::openTag('menupopup');
    	$options .= self::listOptions($selection,$data,$htmlOptions);
    	$options .= self::closeTag('menupopup');
    	
    	if($model->hasErrors($attribute))
    		self::addErrorCss($htmlOptions);
    	
//     	if(isset($htmlOptions['multiple']))
//     	{
//     		if(substr($htmlOptions['name'],-2)!=='[]')
//     			$htmlOptions['name'].='[]';
//     	}
    	
    	return self::tag('menulist',$htmlOptions, $options);
    }
    
    public static function activeRadiogroup($model,$attribute,$data,$htmlOptions=array()){
    	
    	self::resolveNameID($model,$attribute,$htmlOptions);
    	$selection=self::resolveValue($model,$attribute);
    	
    	if($model->hasErrors($attribute))
    		self::addErrorCss($htmlOptions);
    	
    	return self::radiogroup($htmlOptions['id'], $selection, $data, $htmlOptions);    	
    }
    
    /**
     * Generates the list options.
     * @param mixed $selection the selected value(s). This can be either a string for single selection or an array for multiple selections.
     * @param array $listData the option data (see {@link listData})
     * @param array $htmlOptions additional HTML attributes. The following two special attributes are recognized:
     * <ul>
     * <li>encode: boolean, specifies whether to encode the values. Defaults to true.</li>
     * <li>prompt: string, specifies the prompt text shown as the first list option. Its value is empty. Note, the prompt text will NOT be HTML-encoded.</li>
     * <li>empty: string, specifies the text corresponding to empty selection. Its value is empty.
     * The 'empty' option can also be an array of value-label pairs.
     * Each pair will be used to render a list option at the beginning. Note, the text label will NOT be HTML-encoded.</li>
     * <li>options: array, specifies additional attributes for each OPTION tag.
     *     The array keys must be the option values, and the array values are the extra
     *     OPTION tag attributes in the name-value pairs. For example,
     * <pre>
     *     array(
     *         'value1'=>array('disabled'=>true, 'label'=>'value 1'),
     *         'value2'=>array('label'=>'value 2'),
     *     );
     * </pre>
     * </li>
     * <li>key: string, specifies the name of key attribute of the selection object(s).
     * This is used when the selection is represented in terms of objects. In this case,
     * the property named by the key option of the objects will be treated as the actual selection value.
     * This option defaults to 'primaryKey', meaning using the 'primaryKey' property value of the objects in the selection.
     * This option has been available since version 1.1.3.</li>
     * </ul>
     * @return string the generated list options
     */
    public static function listOptions($selection,$listData,&$htmlOptions)
    {		
    	$raw=isset($htmlOptions['encode']) && !$htmlOptions['encode'];
    	$content='';
    	if(isset($htmlOptions['prompt']))
    	{
    		$content.='<menuitem value="">'.strtr($htmlOptions['prompt'],array('<'=>'&lt;', '>'=>'&gt;'))."</menuitem>\n";
    		unset($htmlOptions['prompt']);
    	}
    	if(isset($htmlOptions['empty']))
    	{
    		if(!is_array($htmlOptions['empty']))
    			$htmlOptions['empty']=array(''=>$htmlOptions['empty']);
    		foreach($htmlOptions['empty'] as $value=>$label)
    			$content.='<menuitem value="'.self::encode($value).'">'.strtr($label,array('<'=>'&lt;', '>'=>'&gt;'))."</menuitem>\n";
    		unset($htmlOptions['empty']);
    	}
    
    	if(isset($htmlOptions['options']))
    	{
    		$options=$htmlOptions['options'];
    		unset($htmlOptions['options']);
    	}
    	else
    		$options=array();
    
    	$key=isset($htmlOptions['key']) ? $htmlOptions['key'] : 'primaryKey';
    	if(is_array($selection))
    	{
    		foreach($selection as $i=>$item)
    		{
    			if(is_object($item))
    				$selection[$i]=$item->$key;
    		}
    	}
    	else if(is_object($selection))
    		$selection=$selection->$key;
    
    	
    	$selectedIndex = -1;
    	$index = 0;
    	foreach($listData as $key=>$value)
    	{
    		if(is_array($value))
    		{
    			
    			$dummy=array('options'=>$options);
    			if(isset($htmlOptions['encode']))
    				$dummy['encode']=$htmlOptions['encode'];
    			$content.=self::listOptions($selection,$value,$dummy);
    			
    		}
    		else
    		{
    			$attributes=array('value'=>(string)$key, 'encode'=>!$raw);
    			
    			if(!is_array($selection) && !strcmp($key,$selection) || is_array($selection) && in_array($key,$selection)){
    				$attributes['selected']='true';
    				//$selectedIndex = $index;    				
    			}
    			if(isset($options[$key]))
    				$attributes=array_merge($attributes,$options[$key]);
    			
    			$attributes['label'] = $raw?(string)$value : self::encode((string)$value);
    			
    			$content.=self::tag('menuitem',$attributes);
    		}
    		
    		$index++;
    	}
    	
    	//$htmlOptions['selectedIndex'] = $selectedIndex;
    	
    
    	unset($htmlOptions['key']);
    
    	return $content;
    }
    


    /**
     * Generates a hidden input for a model attribute.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $htmlOptions additional HTML attributes.
     * @return string the generated input field
     * @see activeInputField
     */
    public static function activeHiddenField($model,$attribute,$htmlOptions=array())
    {

        $htmlOptions['style'] = 'display:none;';
        self::resolveNameID($model,$attribute,$htmlOptions);
        return self::activeInputField('textbox',$model,$attribute,$htmlOptions);
    }

    /**
     * Generates a password field input for a model attribute.
     * If the attribute has input error, the input field's CSS class will
     * be appended with {@link errorCss}.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $htmlOptions additional HTML attributes. Besides normal HTML attributes, a few special
     * attributes are also recognized (see {@link clientChange} and {@link tag} for more details.)
     * @return string the generated input field
     * @see clientChange
     * @see activeInputField
     */
    public static function activePasswordField($model,$attribute,$htmlOptions=array())
    {
        self::resolveNameID($model,$attribute,$htmlOptions);
        return self::activeInputField('password',$model,$attribute,$htmlOptions);
    }

    /**
     * Generates a text area input for a model attribute.
     * If the attribute has input error, the input field's CSS class will
     * be appended with {@link errorCss}.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $htmlOptions additional HTML attributes. Besides normal HTML attributes, a few special
     * attributes are also recognized (see {@link clientChange} and {@link tag} for more details.)
     * @return string the generated text area
     * @see clientChange
     */
    public static function activeTextArea($model,$attribute,$htmlOptions=array())
    {
        self::resolveNameID($model,$attribute,$htmlOptions);
        self::clientChange('change',$htmlOptions);
        if($model->hasErrors($attribute))
            self::addErrorCss($htmlOptions);
        $text=self::resolveValue($model,$attribute);
        $htmlOptions['value'] = isset($htmlOptions['encode']) && !$htmlOptions['encode'] ? $text : self::encode($text);
        return self::tag('textbox',$htmlOptions);
    }

    /**
     * Generates an input HTML tag for a model attribute.
     * This method generates an input HTML tag based on the given data model and attribute.
     * If the attribute has input error, the input field's CSS class will
     * be appended with {@link errorCss}.
     * This enables highlighting the incorrect input.
     * @param string $type the input type (e.g. 'text', 'radio')
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $htmlOptions additional HTML attributes for the HTML tag
     * @return string the generated input tag
     */
    protected static function activeInputField($type,$model,$attribute,$htmlOptions)
    {
        $htmlOptions['type']=$type;
        if($type==='text' || $type==='password')
        {
            if(!isset($htmlOptions['maxlength']))
            {
                foreach($model->getValidators($attribute) as $validator)
                {
                    if($validator instanceof CStringValidator && $validator->max!==null)
                    {
                        $htmlOptions['maxlength']=$validator->max;
                        break;
                    }
                }
            }
            else if($htmlOptions['maxlength']===false)
                unset($htmlOptions['maxlength']);
        }

        if($type==='file')
            unset($htmlOptions['value']);
        else if(!isset($htmlOptions['value']))
            $htmlOptions['value']=self::resolveValue($model,$attribute);
        if($model->hasErrors($attribute))
            self::addErrorCss($htmlOptions);
        return self::tag('textbox',$htmlOptions);
    }
    
    /**
     * Generates a check box for a model attribute.
     * The attribute is assumed to take either true or false value.
     * If the attribute has input error, the input field's CSS class will
     * be appended with {@link errorCss}.
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $htmlOptions additional HTML attributes. Besides normal HTML attributes, a few special
     * attributes are also recognized (see {@link clientChange} and {@link tag} for more details.)
     * A special option named 'uncheckValue' is available that can be used to specify
     * the value returned when the checkbox is not checked. By default, this value is '0'.
     * Internally, a hidden field is rendered so that when the checkbox is not checked,
     * we can still obtain the posted uncheck value.
     * If 'uncheckValue' is set as NULL, the hidden field will not be rendered.
     * @return string the generated check box
     * @see clientChange
     * @see activeInputField
     */
    public static function activeCheckBox($model,$attribute,$htmlOptions=array())
    {
    	self::resolveNameID($model,$attribute,$htmlOptions);
    	
    	$value = self::resolveValue($model,$attribute);
    	
    	// TODO valid adatokkal
    	//$value = !($value === 'false' || $value === 0 || $value === false);
    	
    	$label = $model->getAttributeLabel($attribute);
    		
    	return Xul::checkBox($htmlOptions['id'], $label, $value, $htmlOptions);
    }
    
    /**
     * 
     * @param string $id
     * @param boolean $checked
     * @param array $htmlOptions
     */
    public static function checkBox($id, $label = '', $checked = false, $htmlOptions = array()){
    	
    	$htmlOptions['checked'] = ( $checked ? 'true' : 'false' );
    	$htmlOptions['label'] = $label;
    	
    	if(null !== $id){
    		$htmlOptions['id'] = $id;
    	}
    	    	
    	return Xul::tag('checkbox', $htmlOptions);
    }

    /**
     * Displays a summary of validation errors for one or several models.
     * @param mixed $model the models whose input errors are to be displayed. This can be either
     * a single model or an array of models.
     * @param string $header a piece of HTML code that appears in front of the errors
     * @param string $footer a piece of HTML code that appears at the end of the errors
     * @param array $htmlOptions additional HTML attributes to be rendered in the container div tag.
     * A special option named 'firstError' is recognized, which when set true, will
     * make the error summary to show only the first error message of each attribute.
     * If this is not set or is false, all error messages will be displayed.
     * This option has been available since version 1.1.3.
     * @return string the error summary. Empty if no errors are found.
     * @see CModel::getErrors
     * @see errorSummaryCss
     */
    public static function errorSummary($model,$header=null,$footer=null,$htmlOptions=array())
    {
        $content='';
        if(!is_array($model))
            $model=array($model);
        if(isset($htmlOptions['firstError']))
        {
            $firstError=$htmlOptions['firstError'];
            unset($htmlOptions['firstError']);
        }
        else
            $firstError=false;
        foreach($model as $m)
        {
            foreach($m->getErrors() as $errors)
            {
                foreach($errors as $error)
                {
                    if($error!='')
                        $content.="<box>$error</box>\n";
                    if($firstError)
                        break;
                }
            }
        }
        if($content!=='')
        {
            if($header===null)
                $header='<description>'.Yii::t('yii','Please fix the following input errors:').'</description>';
            if(!isset($htmlOptions['class']))
                $htmlOptions['class']=self::$errorSummaryCss;
            return self::tag('vbox',$htmlOptions,$header."\n<vbox>\n$content</vbox>".$footer);
        }
        else
            return '';
    }

    /**
     * Displays the first validation error for a model attribute.
     * @param CModel $model the data model
     * @param string $attribute the attribute name
     * @param array $htmlOptions additional HTML attributes to be rendered in the container div tag.
     * @return string the error display. Empty if no errors are found.
     * @see CModel::getErrors
     * @see errorMessageCss
     */
    public static function error($model,$attribute,$htmlOptions=array())
    {
        self::resolveName($model,$attribute); // turn [a][b]attr into attr
        $error=$model->getError($attribute);
        if($error!='')
        {
            if(!isset($htmlOptions['class']))
                $htmlOptions['class']=self::$errorMessageCss;
            return self::tag('box',$htmlOptions,$error);
        }
        else
            return '';
    }

    /**
     * Generates a valid HTML ID based on name.
     * @param string $name name from which to generate HTML ID
     * @return string the ID generated based on name.
     */
    public static function getIdByName($name)
    {
        return $name;
    }

    /**
     * Generates input name and ID for a model attribute.
     * This method will update the HTML options by setting appropriate 'name' and 'id' attributes.
     * This method may also modify the attribute name if the name
     * contains square brackets (mainly used in tabular input).
     * @param CModel $model the data model
     * @param string $attribute the attribute
     * @param array $htmlOptions the HTML options
     */
    public static function resolveNameID($model,&$attribute,&$htmlOptions)
    {

        if(!isset($htmlOptions['id']))
            $htmlOptions['id']=self::resolveName($model,$attribute);
        else if($htmlOptions['id']===false)
            unset($htmlOptions['id']);

        return $htmlOptions['id'];
    }

    /**
     *
     * @param any $url
     * @return string absolute URL
     */
    public static function normalizeUrl($url){

        if(is_string($url)){

            if(strpos($url, 'http') !== 0){ // this not an absolute url
                $url = '/'.trim($url, '/');

                return Yii::app()->request->hostInfo . $url;
            }


            return $url;

        }else{

            $params = array();
            if(is_array($url)){
                if(count($url) > 1)
                    $params = array_slice($url, 1);

                $url = $url[0];
            }


            return Yii::app()->controller->createAbsoluteUrl($url, $params);

        }
       
    }
    
    /**
     * @param $id string
     * @param $strictMode if true, the result will be true only if the reRender parameter contains the id
     */
    public static function isRequired($id, $strictMode = false){
    	
    	$reRender = isset($_REQUEST['reRender']) ? $_REQUEST['reRender'] : null;

    	if($reRender === null){
    		
    		return $strictMode ? false : true;
    		
    	}else{
    		
    		return strpos($reRender, $id) === false ? false : true; 
    		
    	}
    	
    	
    }
    
    /**
     * Generates an HTML element.
     * @param string $tag the tag name
     * @param array $htmlOptions the element attributes. The values will be HTML-encoded using {@link encode()}.
     * If an 'encode' attribute is given and its value is false,
     * the rest of the attribute values will NOT be HTML-encoded.
     * Since version 1.1.5, attributes whose value is null will not be rendered.
     * @param mixed $content the content to be enclosed between open and close element tags. It will not be HTML-encoded.
     * If false, it means there is no body content.
     * @param boolean $closeTag whether to generate the close tag.
     * @return string the generated HTML element tag
     */
    public static function tag($tag,$htmlOptions=array(),$content=false,$closeTag=true)
    {
    	$html='<' . $tag . self::renderAttributes($htmlOptions);
    	if($content===false)
    		return $closeTag ? $html.' />' : $html.'>';
    	else
    		return $closeTag ? $html.'>'.$content.'</'.$tag.'>' : $html.'>'.$content;
    }
    
    /**
     * Generates an open HTML element.
     * @param string $tag the tag name
     * @param array $htmlOptions the element attributes. The values will be HTML-encoded using {@link encode()}.
     * If an 'encode' attribute is given and its value is false,
     * the rest of the attribute values will NOT be HTML-encoded.
     * Since version 1.1.5, attributes whose value is null will not be rendered.
     * @return string the generated HTML element tag
     */
    public static function openTag($tag,$htmlOptions=array())
    {
    	return '<' . $tag . self::renderAttributes($htmlOptions) . '>';
    }
    
    /**
     * Renders the HTML tag attributes.
     * Since version 1.1.5, attributes whose value is null will not be rendered.
     * Special attributes, such as 'checked', 'disabled', 'readonly', will be rendered
     * properly based on their corresponding boolean value.
     * @param array $htmlOptions attributes to be rendered
     * @return string the rendering result
     */
    public static function renderAttributes($htmlOptions)
    {
    	static $specialAttributes=array(    			
    	);
    
    	if($htmlOptions===array())
    		return '';
    
    	$html='';
    	if(isset($htmlOptions['encode']))
    	{
    		$raw=!$htmlOptions['encode'];
    		unset($htmlOptions['encode']);    		
    	}
    	else
    		$raw=false;
    
    	foreach($htmlOptions as $name=>$value)
    	{
    		if($value!==null)
    			$html .= ' ' . $name . '="' . ($raw ? $value : self::encode($value)) . '"';
    	}
    
    	return $html;
    }
    
    /**
     * 
     * @param CModel $model
     * @return string formatted summary message
     */
    public static function formatErrors(CModel $model){
    	
    	if(!$model->hasErrors()){
    		return '';
    	}
    	
    	$msg = '';
    	
    	foreach ($model->getErrors() as $attribute => $message){
    		$msg .= $model->getAttributeLabel($attribute) . ': ' . $message[0] . "\n"; 
    	}
    	return $msg;
    }


}