<?php

/**
 * 
 * @author Zsolt Lengyel
 *
 */
interface IZooltagController{
	
	public function actionMozBinding($aliases = '');
	public function actionImport($aliases = '');
	
}