<?php

/**
 *
 * Manages the script to run in XULRunner.
 *
 * @author Zsolt Lengyel
 *
 */
class ZoolClientScript extends CClientScript{

	/**
	 * Key in Session.
	 */
	const ZOOL_SESSION_INIT_SCRIPTS = 'zool-session-init-script-installed';
	
    /**
     *
     * @var array structured script map
     */
    protected $lastScripts = array();
    
    /**
     * 
     * @var array
     */
    private $registeredZooltags = array();

    private $_baseUrl;

    public function reset(){
        parent::reset();
        $this->lastScripts = array();
    }

    /**
     * Registers script on start of session.
     * @param string $id
     * @param string $script
     * @param int $position
     */
    public function registerInitScript($id, $script, $position = null){
    
    	$initScripts = &$this->getInitScriptSessionArray();
    	
    	if(!isset($initScripts[$id]) || YII_DEBUG){
    		
    		$this->registerScript($id, $script, $position);
    		
    		$_SESSION[self::ZOOL_SESSION_INIT_SCRIPTS][$id] = true;
    	}
    	
    }
    
    public function registerZooltag($alias){
    	$this->hasScripts = true;
    	$this->registeredZooltags[] = $alias;
    }
    
    /**
     * Registers script on start of session.
     * @param string $id
     * @param string $script
     * @param int $position
     */
    public function registerInitScriptFile($url,  $position = null){
    	     	
    	$initScripts = $this->getInitScriptSessionArray();
    	
    	if(!isset($initScripts[$url]) || YII_DEBUG){
    
    		$this->registerScriptFile($url, $position);
    
    		$_SESSION[self::ZOOL_SESSION_INIT_SCRIPTS][$url] = true;
    	}
    	 
    }
    
    public function registerInitCssFile($url,  $position = null){
    	 
    	$initScripts = $this->getInitScriptSessionArray();
    	 
    	if(!isset($initScripts[$url]) || YII_DEBUG){
    
    		$this->registerCssFile($url, $position);
    
    		$_SESSION[self::ZOOL_SESSION_INIT_SCRIPTS][$url] = true;
    	}
    
    }
    
    /**
     * 
     * @return array <>
     */
    protected function getInitScriptSessionArray() {
    	$session = Yii::app()->getSession();
    	    	 
    	if(null === $session[self::ZOOL_SESSION_INIT_SCRIPTS]){
    		$session[self::ZOOL_SESSION_INIT_SCRIPTS] = array();
    	}
    	 
    	return $session[self::ZOOL_SESSION_INIT_SCRIPTS];
    }

    /**
     * Renders the registered scripts.
     * This method is called in {@link CController::render} when it finishes
     * rendering content. CClientScript thus gets a chance to insert script tags
     * at <code>head</code> and <code>body</code> sections in the HTML output.
     * @param string $output the existing output that needs to be inserted with script tags
     */
    public function render(&$output)
    {
        if(Yii::app()->getModule('zool')->outputFormat != 'json'){
            parent::render($output);
            return;
        }

        if(!$this->hasScripts)
            return;

        $this->renderCoreScripts();

        if(!empty($this->scriptMap))
            $this->remapScripts();

        $this->unifyScripts();

        $this->renderHead($output);
        if($this->enableJavaScript)
        {
            $this->renderBodyBegin($output);
            $this->renderBodyEnd($output);
        }
    }

    /**
     * Inserts the scripts in the head section.
     * @param string $output the output to be inserted with scripts.
     */
    public function renderHead(&$output)
    {

        // META tags
        if(count($this->metaTags) > 0)
            $this->lastScripts['meta'] = array();
        foreach($this->metaTags as $meta)
            $this->lastScripts['meta'][]=$meta;

        // LINK tags
        if(count($this->linkTags) > 0)
            $this->lastScripts['link'] = array();
        foreach($this->linkTags as $link)
            $this->lastScripts['link'][]=$link;
        
        // register zooltags
        if(!empty($this->registeredZooltags)){
        	
        	$aliases = implode(',', $this->registeredZooltags);        	
        	$provider = Yii::app()->getModule('zool')->zooltagProviderUrl . '/mozBinding';        	
        	$bindingUrl = Yii::app()->getController()->createAbsoluteUrl($provider, array('aliases'=>$aliases));
        	
        	$this->cssFiles[$bindingUrl] = 'zooltag';
        	
        }

        // CSS files
        if(count($this->cssFiles) > 0)
            $this->lastScripts['cssFiles'] = array();
        foreach($this->cssFiles as $url=>$media){
            $this->lastScripts['cssFiles'][]=$this->createAbsoluteUrl($url);
        }

        // CSS script
        if(count($this->css) > 0)
            $this->lastScripts['css'] = array();
        foreach($this->css as $css)
            $this->lastScripts['css'][]=$css;

        if($this->enableJavaScript)
        {

            if(!isset($this->lastScripts['scriptFiles']))
                $this->lastScripts['scriptFiles'] = array();

            if(isset($this->scriptFiles[self::POS_HEAD]))
            {

                $this->lastScripts['scriptFiles']['POS_HEAD'] = array();
                foreach($this->scriptFiles[self::POS_HEAD] as $scriptFile)
                    $this->lastScripts['scriptFiles']['POS_HEAD'][]=$this->createAbsoluteUrl($scriptFile);
            }

            if(isset($this->scripts[self::POS_HEAD])){
                if(!isset($this->lastScripts['scripts']))
                    $this->lastScripts['scripts'] = array();

                $this->lastScripts['scripts']['POS_HEAD']= implode("\n",$this->scripts[self::POS_HEAD]);
            }
        }

    }

    /**
     * Inserts the scripts at the beginning of the body section.
     * @param string $output the output to be inserted with scripts.
     */
    public function renderBodyBegin(&$output)
    {
        if(isset($this->scriptFiles[self::POS_BEGIN]))
        {
            if(!isset($this->lastScripts['scriptFiles']))
                $this->lastScripts['scriptFiles'] = array('POS_BEGIN'=>array());

            foreach($this->scriptFiles[self::POS_BEGIN] as $scriptFile)
                $this->lastScripts['scriptFiles']['POS_BEGIN'][] = $scriptFile;
        }

        if(isset($this->scripts[self::POS_BEGIN])){
            if(!isset($this->lastScripts['scripts']))
                $this->lastScripts['scripts'] = array();

            $this->lastScripts['scripts']['POS_BEGIN']=implode("\n",$this->scripts[self::POS_BEGIN]);
        }

    }

    /**
     * Inserts the scripts at the end of the body section.
     * @param string $output the output to be inserted with scripts.
     */
    public function renderBodyEnd(&$output)
    {
        if(!isset($this->scriptFiles[self::POS_END]) && !isset($this->scripts[self::POS_END])
                && !isset($this->scripts[self::POS_READY]) && !isset($this->scripts[self::POS_LOAD]))
            return;

        if(isset($this->scriptFiles[self::POS_END]))
        {
            if(!isset($this->lastScripts['scriptFiles']))
                $this->lastScripts['scriptFiles'] = array('POS_END'=>array());

            foreach($this->scriptFiles[self::POS_END] as $scriptFile)
                $this->lastScripts['scriptFiles']['POS_END'][] = $scriptFile;
        }

        $scripts=isset($this->scripts[self::POS_END]) ? $this->scripts[self::POS_END] : array();

        if(isset($this->scripts[self::POS_READY]))
        {
            $scripts[]=implode("\n",$this->scripts[self::POS_READY]);
        }
        if(isset($this->scripts[self::POS_LOAD]))
        {
            $scripts[]=implode("\n",$this->scripts[self::POS_LOAD]);
        }

        if(!empty($scripts)){
            if(!isset($this->lastScripts['scripts']))
                $this->lastScripts['scripts'] = array();

            $this->lastScripts['scripts']['POS_LOAD']=implode("\n",$scripts);
        }

    }

    /**
     * Returns the base URL of all core javascript files.
     * If the base URL is not explicitly set, this method will publish the whole directory
     * 'framework/web/js/source' and return the corresponding URL.
     * @return string the base URL of all core javascript files
     */
    public function getCoreScriptUrl()
    {
        if($this->_baseUrl!==null)
            return $this->_baseUrl;
        else
            return $this->_baseUrl=Yii::app()->getRequest()->getServerName() . Yii::app()->getAssetManager()->publish(YII_PATH.'/web/js/source');
    }

    /**
     *
     * @return array last rendered scritps
     */
    public function getLastScripts(){
        return $this->lastScripts;
    }

    /**
     * Makes absolute url from relative or absolute.
     *
     * @param string $url
     * @return string
     */
    private function createAbsoluteUrl($url){
        if(strpos($url, 'http') === 0){
            return $url;
        }
        return Yii::app()->getRequest()->getHostInfo('') . $url;
    }


}