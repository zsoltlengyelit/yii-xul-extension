<?php

ZoolModule::import('zooltag.*');

/**
 * 
 * @author Zsolt Lengyel
 *
 */
class ZooltagController extends ZoolController implements IZooltagController{
	
	private $tags = array();
	
	public function actionMozBinding($aliases = ''){
		
		$tagAliases = array();
		
		if(strpos($aliases, ',') === false){
			$tagAliases[0] = $aliases;
		}else{
			$tagAliases = explode(',', $aliases);
		}
		
		foreach ($tagAliases as $alias){

			list($lib, $tag) = Zooltag::info($alias);
			
			$url = $this->createAbsoluteUrl('zooltag/import', array('aliases'=>$alias));
			
			echo "$tag {
				-moz-binding: url('". $url ."#$tag');
			}";
			
		}
		
	}
	
	public function actionImport($aliases = ''){
				
		$tagAliases = array();
		
		if(strpos($aliases, ',') === false){
			$tagAliases[0] = $aliases;
		}else{			
			$tagAliases = explode(',', $aliases);			
		}
		
		foreach ($tagAliases as $alias){			
			$this->renderTag($alias);			
		}
		
		$content = '';
		foreach ($this->tags as $tag){
			ob_start();
			$tag->render();
			
			$content .= ob_get_clean();  
		}
		
		
		$this->renderPartial('bindings', array('content'=> $content));		
	}
	
	public function createTag($name, $options = array(), $implements = null){
		
		$tag = new Zooltag($name, $options, $implements);

		$this->tags[] = $tag;
		
		return $tag;
	}
	
	
	private function renderTag($tagname){
		
		$tagalias = $this->getTagPathAlias($tagname);
		
		$tagContent = $this->renderPartial($tagalias, array(), true);
		
		$tagContent .= "\n\n";
		
		return $tagContent;
		
	}
	
	private function getTagPathAlias($tagname){
		
		list($libname, $tag) = Zooltag::info($tagname);
		
		foreach ($this->getZooltagsPaths() as $lib => $alias){
			
			$tagalias = trim($alias, '.') . '.' .$tag;
			
			$realpath = Yii::getPathOfAlias($tagalias) . '.php';
			
			if($lib == $libname && is_file($realpath)){				
				return $tagalias;				
			}
		}
		
		throw new ZoolException('Unknown tag: '. $tagname);	
			
	}
	
	private function getZooltagsPaths(){
		return Yii::app()->getModule('zool')->getZooltagsPaths();
	}
	
	//public function render($)
	
}