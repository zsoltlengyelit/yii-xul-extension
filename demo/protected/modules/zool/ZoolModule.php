<?php

/**
 *
 * Module for create amazing web appliactions with XUL-based front end.
 *
 * @author Zsolt Lengyel
 *
 */
class ZoolModule extends CWebModule{

    const MODULE_NAME = 'zool';

    const POSSIBLE_OUTPUT_FORMATS = 'json,xml';
    
    public $zooltagProviderUrl = '/zool/zooltag';
    
    /**
     * If true, only the required widgets, and parts be be rendered.
     * @var boolean
     */
    // TODO ez nem működő funkció
    private $_fastRender = true;

    /**
     * Possible values : json, xml
     *
     * @var string
     */
    private $_outputFormat = 'json';
    
    private $_xblNamespaceName = 'xbl';
    
    
    
    /**
     * 
     * @var array alias of path of custom Zool tags.
     */
    private $_zooltagsPaths = array();
    
    private $prefixes = array();
    
   
    
    public function init(){
    	
    	$this->_zooltagsPaths = array(
    			'application' => 'application.components.zooltags',
    			'zool' => 'zool.components.zooltags',    			
    			);
    	
    }

    /**
     *
     * @param string $pathAlias
     */
    public static function import($pathAlias){
        $path = 'application.modules.'. self::MODULE_NAME . '.' . $pathAlias;

        Yii::import($path);
    }
    
    /**
     * 
     * @return array
     */
    public function getPrefixes(){
    	return $this->prefixes;
    }
    
    /**
     *
     * @param string $key
     * @param mixed $value
     * @throws ZoolException when key is not string
     */
    public function setPrefix($key, $value){
    	if(!is_string($key)){
    		throw new ZoolException('Prefix key must be string.');
    	}
    	$this->prefixes[$key] = $value;
    }
    
    /**
     *
     * @return string outfput format
     */
    public function getOutputFormat(){
        return $this->_outputFormat;
    }

    /**
     *
     * @param string $outputFormat output format
     * @throws ZoolException
     */
    public function setOutputFormat($outputFormat){

        if(in_array($outputFormat, explode(',', self::POSSIBLE_OUTPUT_FORMATS))){
            $this->_outputFormat = $outputFormat;
        }else{
            throw new ZoolException("Invalid output format: " . $outputFormat );
        }

    }
    
    /**
     * 
     * @return boolean
     */
    public function getFastRender(){
    	return $this->_fastRender;
    }
    
    /**
     * 
     * @param unknown_type $fastRender
     */
    public function setFastRender($fastRender){
    	$this->_fastRender = $fastRender;
    }

    /**
     * Absolute path to server.
     * @return <string, NULL>
     */
    public function getServerUrl(){
        return Yii::app()->getRequest()->getHostInfo('') . Yii::app()->baseUrl;
    }
    
    /**
     * 
     * @return array
     */
    public function getZooltagsPaths(){
    	return $this->_zooltagsPaths;
    }
    
    /**
     * 
     * @param array $path
     */
    public function setZooltagsPaths($paths){
    	$this->_zooltagsPaths = CMap::mergeArray($this->_zooltagsPaths, $paths);
    }
    public function getXblNamespaceName(){
    	return $this->_xblNamespaceName;
    }
    
    public function setXblNamespaceName($name){
    	$this->_xblNamespaceName = $name;
    }
    
    /**
     * 
     * @param string $id Id of widget, or any element
     */
    public function shouldRender($id){
    	
    	$id = trim($id);
    	
    	// no prefered ID sure should render
    	if(!isset($_REQUEST['reRender']))
    		return true;
    	
    	$reRender = $_REQUEST['reRender'];
    	
    	if(empty($reRender))
    		return true;
    	
    	if(strpos($reRender, ',') === false){ // no comma, no multiple elem
    		
    		$reRender = trim($reRender);
    		
    		if($reRender == $id)
    			return true;
    		
    	}else{
    		
    		$reRender = explode(',', $reRender);
    		$reRender = array_map('trim', $reRender);
    		
    		return in_array($id, $reRender);
    		
    	}
    }

}