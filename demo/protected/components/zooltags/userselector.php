<?php
/* @var $this ZooltagController */
/* @var $tag Zooltag */

$tag = $this->createTag('userselector');

	//---------------- properties -------------------
	$name = $tag->createProperty('value');
		$name->beginSetter(); ?>
			this.setAttribute('value', val);
		<?php $name->endSetter();
		
		$name->beginGetter(); ?>
			return this.getAttribute('value');
		<?php $name->endGetter();
	
	//----------------- end of properties ------------
	
	//------------------ methods ---------------------
	$tag->beginConstructor(); ?>
		
		App.inputHandlers.userselector = function(elem){
			return elem.value;
		};
		
		this.jsonUrl = "<?php echo $this->createAbsoluteUrl('/userselector/suggest');?>";
		
	<?php $tag->endConstructor();
	
	$refresh = $tag->createMethod('refresh')->setBody(
		<<<BODY
			
		let userlist = Zool.byId('userlist');
			
		Sys.ajax({
			url : this.jsonUrl,
			parse: 'json',
			data: {filter: document.getElementById('usertext').value},
			success: function(data){
			
				Zool.removeChildren(userlist);
			
				for(var i=0; i < data.length; i++){
					var user = data[i];
					document.getBindingParent(this).addOption(user);
				}
				
				Sys.log(data);
				d(data);
			},
			
			error: function(e){
				window.alert(e);
			}
		});
BODY
);

	$addOption = $tag->createMethod('addOption', array('user'))->setBody(
		<<<BODY
		let popup = Zool.byId("userlist");
		let menuitem = document.createElement('menuitem');
		menuitem.label = user.name;
		menuitem.value = user.id;
		
		popup.appendChild(menuitem);		
BODY
);
	
	// ------------------- focus method --------------------
	$focus = $tag->createMethod('focus')->setBody(
		<<<BODY
		let popup = Zool.byId("userlist");
		//popup.openPopup(Zool.byId("usertext"));
BODY
);

	$focus = $tag->createMethod('blur')->setBody(
		<<<BODY
		let popup = Zool.byId("userlist");
		//popup.hidePopup();
BODY
);
		
	
	// ----------------- content ----------------------------
		
	$tag->beginContent();
	
		echo Xul::beginBox(array(
			'id'=>'selectorbox',
			));

			echo Xul::textbox('usertext', '', array(
				'onfocus' => 'document.getBindingParent(this).focus()',
				'onblur' => 'document.getBindingParent(this).blur()',
				'oninput' => 'document.getBindingParent(this).refresh()',

			));
		
		echo Xul::endBox();
		
		echo Xul::openTag('menulist');
		echo Xul::openTag('menupopup', array('id'=>'userlist'));
		echo Xul::closeTag('menupopup');
		echo Xul::closeTag('menulist');
	
	$tag->endContent();
