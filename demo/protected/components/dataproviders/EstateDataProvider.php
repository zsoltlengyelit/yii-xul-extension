<?php

/**
 * 
 * @author Zsolt Lengyel
 *
 */
class EstateDataProvider extends CActiveDataProvider{

	private $onlyOwneds = true;
	

	/**
	 *
	 * @var EstateSearch
	 */
	private $_filter = null;
	

	/**
	 * 
	 * @param boolean $owned true means result contiains only managed estates
	 */
	public function __construct($owned = true, $pageSize = 10){

		parent::__construct('Estate', array(
				'pagination'=>array(
						'pageSize' => $pageSize
				)
		));
		
		$this->onlyOwneds = $owned;

	}
	
	/**
	 * (non-PHPdoc)
	 * @see CActiveDataProvider::getCriteria()
	 */
	public function getCriteria(){
		$criteria = parent::getCriteria();
		
		if($this->onlyOwneds){			
			$criteria->addCondition('agent_id = :userId');
		}else{
			$criteria->addCondition('agent_id != :userId');
		}		
		$criteria->params[':userId'] = Yii::app()->user->id;
		
		
		//
		$filter = $this->getFilter()->filter;
		
		if(!empty($filter)){
			
			$search = new CDbCriteria();
			
			$search->addSearchCondition('title', $filter, true, 'OR');
			$search->addSearchCondition('description', $filter, true, 'OR');
			$search->addSearchCondition('address', $filter, true, 'OR');
			
			$criteria->mergeWith($search);
		}
		
		$sold = $this->getFilter()->sold;
		
		if($sold != ''){
			
			$criteria->addCondition('sold = :soldParam');
			$criteria->params[':soldParam'] = $sold == 'true' ? '1' : '0';
			
		}
		
		return $criteria;
		
	}
	
	/**
	 * 
	 * @return EstateSearch
	 */
	public function getFilter(){
		
		if($this->_filter == null){
			$this->_filter = new EstateSearch();
		
			if(isset($_POST['EstateSearch'])){
				$this->_filter->filter = $_POST['EstateSearch']['filter'];
				$this->_filter->sold = $_POST['EstateSearch']['sold'];
				
				
			}
		}
		
		return $this->_filter;
		
	}

}