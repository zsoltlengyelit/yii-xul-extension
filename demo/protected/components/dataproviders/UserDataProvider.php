<?php

/**
 *
 * @author Zsolt Lengyel
 *
 */
class UserDataProvider extends CActiveDataProvider{

	private $justothers = false;
	
	private $_filter = null;
	
    /**
     *
     */
    public function __construct($justothers = true){

        parent::__construct('User', array(
                'pagination'=>array(
                        'pageSize' => 20
                        )
                ));
        
        $this->justothers = $justothers;

    }
    
    /**
     * (non-PHPdoc)
     * @see CActiveDataProvider::getCriteria()
     */
    public function getCriteria(){
    	$criteria = parent::getCriteria();
    
    	if($this->justothers){
    			
    		$criteria->addCondition('id <> :userId');
    		$criteria->params['userId'] = Yii::app()->user->id;
    	}
    	
    	$filter = $this->getFilter()->filter;
    	
    	if(!empty($filter)){
    		
    		$search = new CDbCriteria();
			
			$search->addSearchCondition('username', $filter, true, 'OR');
			$search->addSearchCondition('fullname', $filter, true, 'OR');
			$search->addSearchCondition('email', $filter, true, 'OR');
			
			$criteria->mergeWith($search);
    		
    	}
    
    	return $criteria;
    
    }
    
    /**
     *
     * @return UserSearch
     */
    public function getFilter(){
    
    	if($this->_filter == null){
    		$this->_filter = new UserSearch();
    
    		if(isset($_POST['UserSearch'])){
    			$this->_filter->filter = $_POST['UserSearch']['filter'];    
    		}
    	}
    
    	return $this->_filter;
    
    }

}