<?php

/**
 * 
 * @author Zsolt Lengyel
 *
 */
class CustomerDataProvider extends CActiveDataProvider{
	
	/**
	 *
	 * @var CustomerFilter
	 */
	private $_filter = null;
	
	/**
	 * 
	 */
	public function __construct($pageSize = 30){
		parent::__construct('Customer', array(
				'pagination'=>array(
						'pageSize' => $pageSize
				)
		));
	}
	
	/**
	 * (non-PHPdoc)
	 * @see CActiveDataProvider::getCriteria()
	 */
	public function getCriteria(){
		$criteria = parent::getCriteria();
			
		$filter = $this->getFilter()->filter;
		 
		if(!empty($filter)){
	
			$search = new CDbCriteria();
				
			$search->addSearchCondition('name', $filter, true, 'OR');
			$search->addSearchCondition('phone', $filter, true, 'OR');
			$search->addSearchCondition('address', $filter, true, 'OR');
				
			$criteria->mergeWith($search);
	
		}
	
		return $criteria;
	
	}
	
	
	/**
	 *
	 * @return UserSearch
	 */
	public function getFilter(){
	
		if($this->_filter == null){
			$this->_filter = new CustomerFilter();
	
			if(isset($_POST['CustomerFilter'])){
				$this->_filter->filter = $_POST['CustomerFilter']['filter'];
			}
		}
	
		return $this->_filter;
	
	}
}