<?php

Yii::import('application.models.*');
Yii::import('application.components.*');
Yii::import('application.components.fileimagebehavior.*');
Yii::import('application.extensions.*');
Yii::import('application.extensions.image.*');

class m130503_150555_test_data extends CDbMigration
{

	private $users = array();

	private $customers = array();
	
	private $comforts = array();

	public function up()
	{

		$this->generateUsers();
		$this->generateCustomer();
		$this->generateComforts();

		$this->generateEstates();
	
	}


	private function generateUsers(){

		
		
		$this->users[] = $user = new User();

		$user->username = 'admin';
		$user->password = 'admin';
		$user->fullname = 'Admin';
		$user->active = true;
		$user->email = 'admin@estaetagency.hu';
		$user->agent = true;

		$user->save();


		for($i = 0; $i < 30; $i++){

			$user = new User();

			$user->username = 'user'.$i;
			$user->password = 'user'.$i;
			$user->fullname = $this->generateName();
			$user->active = true;
			$user->agent = $i < 11;
			$user->email = 'user'.$i .'@estaetagency.hu';

			$user->save();
			
			if($user->agent)
				$this->users[] = $user;

		}

	}

	private function generateComforts(){
		
		$comfortNames = array('garden', 'lift', 'parking place', 'climate', 'root garder', 'balcony', 'furnished');
		
		foreach ($comfortNames as $comfortName){
			
			$this->comforts[] = $comfort = new Comfort();
			
			$comfort->name = $comfortName;
			$comfort->save();
			
		}
		
	}
	

	private function generateEstates(){

		$titles = array('Apartment for 2 person', 'Estate in the center', 'House next to the lake', 'Cottage in the forest', 'Garage next to the storehouse', 'Bungallow',
				'Hut for hunters', 'Weekend house', 'Garden house', 'Family house in the garden city');

		$heatings = Estate::heatings();		
		$states = Estate::states();
		
		for($i = 0; $i < 5000; $i++){

			$estate = new Estate();
			

			$estate->agent_id = $this->users[array_rand($this->users)]->id;
			$estate->customer_id = $this->customers[array_rand($this->customers)]->id;				
			$estate->comforts = $this->randomComforts();

			$estate->title = $titles[array_rand($titles)];
			$estate->description = '';
			$estate->address = $this->generateAddress();
			$estate->sold = rand(0, 1);
			$estate->heating = $heatings[array_rand($heatings)];
			$estate->lift = rand(0, 1);
			$estate->size = rand(10, 500);
			$estate->state = $states[array_rand($states)];
			$estate->price = rand(2000, 1000000);	
			
			$estate->save();
		}

	}

	private function randomComforts(){
		
		$max = rand(2, count($this->comforts) - 1);
		
		$indexes = array_rand($this->comforts, $max);
		
		$comfortIds = array();
		
		foreach ($indexes as $index){
			$comfortIds[] = $this->comforts[$index];
		}
		
		return $comfortIds;
		
	}
	
	private function generateCustomer(){


		
		for($i = 0; $i < 50; $i++){

			$this->customers[] = $customer = new Customer();
			$customer->name = $this->generateName();
			$customer->phone = $this->generatePhonenumber();
			$customer->address = $this->generateAddress();

			$customer->save();

		}

	}
	
	private function generateName(){
		$firstnames = array('Jonh', 'Ken', 'William', 'George', 'Arnold', 'Peter', 'Adam', 'Steve', 'Marco', 'Julia', 'Ann', 'Betty', 'Brad', 'Monica', 'Zsazsa', 'Lui', 'Belle', 'Murinho');
		$lastnames = array('Smith', 'Kennedy', 'George', 'McKensie', 'Johnson', 'Jones', 'Anderson', 'White', 'Harris', 'Moore', 'Jackson', 'O\'hara', 'McGiver', 'Put', 'Krawesky', 'Grabowsky', 'Fritz', 'Tyler');
		
		return $firstnames[array_rand($firstnames)] . ' ' . $lastnames[array_rand($lastnames)];
		
	}

	private function generateAddress(){

		$cities = array('Los Angeles', 'London', 'Paris', 'Tokyo', 'Budapest', 'Sydney', 'Singapore', 'Mexico City');
		$streets = array('1. str.', 'St. Jonhs str.', 'Michael', 'Trafalgar', 'Smith steet', 'Chi-pen', 'Kossuth L. road');

		return $streets[array_rand($streets)] . ' ' . rand(1, 300) .', ' .$cities[array_rand($cities)];

	}

	private function generatePhonenumber(){

		$providers = array(20, 30, 70);
		return '+36 ' . $providers[array_rand($providers)] . ' ' . rand(100, 999) . ' ' . rand(1000, 9999);

	}



	public function down(){

		$this->truncateTable('user');
		$this->truncateTable('customer');
		$this->truncateTable('estate');

	}



}