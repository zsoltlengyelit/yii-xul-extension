<?php

class m130503_145720_models extends CDbMigration
{
	public function up()
	{
		
		// ----------- User -------------

		$this->createTable('user', array(
				'id'=> 'pk',
				'username'=> 'string NOT NULL',
				'password'=> 'string NOT NULL',
				'email'=> 'string NOT NULL',
				'fullname'=>'string NOT NULL',
				'active'=>'boolean',
				'agent'=>'boolean DEFAULT \'0\'',
		));
		$this->createIndex('unique_user', 'user', 'username', true);
		$this->createIndex('unique_email', 'user', 'email', true);
		
		// ----------- Customer -------------
		
		$this->createTable('customer', array(
				'id'=>'pk',
				'name'=>'string NOT NULL',
				'phone'=>'string',
				'address'=>'string'
		));
		
		// ----------- Comfort -------------
		
		$this->createTable('comfort', array(
				'id'=>'pk',
				'name'=>'VARCHAR (255) NOT NULL',
		));		
		
		// ----------- Estate -------------
		
		$this->createTable('estate', array(
				'id'=>'pk',
				'title'=>'string NOT NULL',
				'description'=>'string',
				'address'=>'string NOT NULL',
				'agent_id'=>'integer',
				'customer_id'=>'integer',
				'sold'=> 'tinyint(1) DEFAULT \'0\'',
				'created' => 'datetime not null',
				'heating' => 'varchar(255) not null',
				'lift' => 'tinyint(1) default \'0\' not null',
				'size' => 'int(3)',
				'state' => 'varchar(100)',
				'price' => 'int(11) DEFAULT \'0\'',
		));
		
		$this->addForeignKey('estate_agent', 'estate', 'agent_id', 'user', 'id');		
		$this->addForeignKey('estate_customer', 'estate', 'customer_id', 'customer', 'id');
		
		// ----------- Relation -------------
		
		$this->createTable('estate_comfort', array(
				'estate_id' => 'int(11)',
				'comfort_id' => 'int(11)'
		));
		$this->addForeignKey('fk_estate_comfort_estate', 'estate_comfort', 'estate_id', 'estate', 'id');
		$this->addForeignKey('fk_estate_comfort_comfort', 'estate_comfort', 'comfort_id', 'comfort', 'id');
		
		
		
	}

	public function down()
	{
		echo "m130503_145720_models does not support migration down.\n";
		return false;
	}

}