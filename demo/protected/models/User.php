<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $fullname
 * @property string $email
 * @property integer $active
 * @property bool $agent
 *
 * 
 */
class User extends ActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user';
    }

    public function scopes()
    {
        return array(
                'active'=>array(
                        'condition'=>'active=1',
                ),
        		'agent'=>array(
        				'condition'=>'agent=1',
        			)

        );
    }
    
    public function behaviors(){
    	return array(
    			
    			'imageBehavior' => array(
    					'class' => 'ImageARBehavior',
    					'attribute' => 'image', // this must exist
    					'extension' => 'png, gif, jpg', // possible extensions, comma separated
    					'prefix' => 'img_',
    					'relativeWebRootFolder' => 'images/users', // this folder must exist
    			
    					// it is located in /usr/bin on my computer.
    			
    					// this will define formats for the image.
    					// The format 'normal' always exist. This is the default format, by default no
    					// suffix or no processing is enabled.
    					'formats' => array(
    							// create a thumbnail grayscale format
    							'thumb' => array(
    									'suffix' => '_thumb',
    									'process' => array('resize' => array(60, 60), 'grayscale' => false),
    							),
    							// create a large one (in fact, no resize is applied)
    							'large' => array(
    									'suffix' => '_large',
    							),    							
    					),
    			
    					'defaultName' => 'default', // when no file is associated, this one is used
    					// defaultName need to exist in the relativeWebRootFolder path, and prefixed by prefix,
    					// and with one of the possible extensions. if multiple formats are used, a default file must exist
    					// for each format. Name is constructed like this :
    					//     {prefix}{name of the default file}{suffix}{one of the extension}
    			)
    			);
    	
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array_merge(parent::rules(), array(
                array('username, fullname, password, email, active', 'required',  'message'=>'Kötelező kitölteni'),
                array('active', 'numerical', 'integerOnly'=>true),
                array('username', 'length', 'max'=>10),
                array('email, password', 'length', 'max'=>255),
                array('email', 'email', 'message'=>'Nem valódi e-mail cím.'),

                array('username, email, active', 'safe', 'on'=>'search'),
        ));
    }

    public function beforeValidate(){
    	
    	$this->active = ($this->active == 'true' || 1 == $this->active) ? 1 : 0;
    	$this->agent = ($this->agent == 'true' || 1 == $this->agent) ? 1 : 0;
    	
    	return parent::beforeValidate();
    }
    
    /**
     *
     * @see CActiveRecord::beforeSave()
     */
    public function beforeSave(){
        //if($this->isNewRecord){
        $this->password = $this->makePassword($this->password);
        //}

        return parent::beforeSave();;
    }



    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
                'id' => 'ID',
                'username' => 'Felhasználónév',
                'password' => 'Jelszó',
                'email' => 'E-mail',
                'active' => 'Aktív',
        );
    }

    public function hashPassword($pass){
        return $this->password = $this->makePassword($pass);
    }

    /**
     *
     * @param string $pass password
     * @return string hashed password
     */
    public function makePassword($pass){
        return md5(Yii::app()->params['salt'] . $pass);
    }

    public function searchFields()
    {
        return array('username', 'email');
    }
}