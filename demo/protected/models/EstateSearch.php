<?php

/**
 * 
 * @author Zsolt Lengyel
 *
 */
class EstateSearch extends CFormModel{
	
	public $filter;
	
	private $_sold = null;
	
	public function setSold($sold){
		$this->_sold = $sold;
	}
	
	public function getSold(){
		return (string)$this->_sold;
	}
	
}