<?php

/**
 * This is the model class for table "estate".
 *
 * The followings are the available columns in table 'estate':
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $address
 * @property integer $agent_id
 * @property integer $customer_id
 * @property integer $sold
 * @property integer $price price in USD
 * 
 * @property integer $size
 * @property bool $lift
 * @property string $heating
 * @property DateTime $created
 * @property string state
 *
 * The followings are the available model relations:
 * @property Customer $customer
 * @property User $agent
 * @property Comfort[] $comforts
 */
class Estate extends ActiveRecord
{
	
	
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Estate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public static function states(){
		return array('new', 'almost new', 'old', 'renewal recommended', 'good', 'bad');
	}
	
	/**
	 * 
	 * @return array:string
	 */
	public static function statesMap(){
		$ret = array();
		foreach (self::states() as $val){
			$ret[$val] = $val;
		}
		
		return $ret;
	}
	
	public static function heatings(){
		return array('gas', 'center', 'floor', 'wood', 'mixed', 'remote');
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'estate';
	}
		
	public function behaviors()
	{
	    return array(
	        'activerecord-relation'=>array(
	            'class'=>'application.extensions.EActiveRecordRelationBehavior',
	    	),
    		
    		'imageBehavior' => array(
					'class' => 'ImageARBehavior',
					'attribute' => 'image', // this must exist
					'extension' => 'png, gif, jpg', // possible extensions, comma separated
					'prefix' => 'img_',
					'relativeWebRootFolder' => 'images/estates', // this folder must exist

    				// it is located in /usr/bin on my computer.

					// this will define formats for the image.
					// The format 'normal' always exist. This is the default format, by default no
					// suffix or no processing is enabled.
					'formats' => array(
							// create a thumbnail grayscale format
							'thumb' => array(
									'suffix' => '_thumb',
									'process' => array('resize' => array(60, 60), 'grayscale' => true),
							),
							// create a large one (in fact, no resize is applied)
							'large' => array(
									'suffix' => '_large',
							),
							// and override the default :
							'normal' => array(
									'process' => array('resize' => array(200, 200)),
							),
					),

					'defaultName' => 'default', // when no file is associated, this one is used
					// defaultName need to exist in the relativeWebRootFolder path, and prefixed by prefix,
					// and with one of the possible extensions. if multiple formats are used, a default file must exist
					// for each format. Name is constructed like this :
					//     {prefix}{name of the default file}{suffix}{one of the extension}
			));
	}


	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{			
		return array(
			array('title, address', 'required'),
			array('agent_id, customer_id', 'numerical', 'integerOnly'=>true),
			array('title, description, address', 'length', 'max'=>255),
			array('state','in','range'=>self::states() ,'allowEmpty'=>true),
			array('size','numerical', 'integerOnly'=>true,	'min'=>1, 'max'=>280, 
					'tooSmall'=>'The size is at least 1 square meter', 
					'tooBig'=>'Maximum 250 square meter'),
			
			array('id, title, description, address, agent_id, customer_id, sold, state, size, comforts, price', 'safe'),
			array('id, title, description, address, agent_id, customer_id, sold, state, size, comforts, price', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{	
		return array(
			'customer' => array(self::BELONGS_TO, 'Customer', 'customer_id'),
			'agent' => array(self::BELONGS_TO, 'User', 'agent_id'),
			'comforts' => array(self::MANY_MANY, 'Comfort', 'estate_comfort(estate_id, comfort_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'description' => 'Description',
			'address' => 'Address',
			'agent_id' => 'Agent',
			'customer_id' => 'Customer',
		);
	}
	
	public function beforeSave() {
		if ($this->isNewRecord)
			$this->created = new CDbExpression('NOW()');
	
		return parent::beforeSave();
	}
	
	public function hasComfort(Comfort $otherComfort) {		
		
		if(empty($this->comforts)) return false;
		
		foreach ($this->comforts as $comfort){
			if($comfort->equals($otherComfort))
				return true;
		}
		return false;	
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('agent_id',$this->agent_id);
		$criteria->compare('customer_id',$this->customer_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}