
var Zool = {};

Zool.forms = {};

/**
 * 
 */
var NAMESPACES = {
	XUL :  'http://www.mozilla.org/keymaster/gatekeeper/there.is.only.xul',
	HTML : 'http://www.w3.org/1999/xhtml'	
};

Zool.byId = function(id) {
	return document.getElementById(id);
};


Zool.valueById = function(id) {
	var el = Zool.byId(id);
	var val = el.value;

	return val;
};

/**
 * 
 */
Zool.inputValueGetter = {
		
		input : function(elem){
			
		}
		
		
};

Zool.openDialog = function(url, mode) {
	window
			.openDialog(
					'chrome://myapp/content/dialog.xul',
					'showmore',
					mode || 'chrome, centerscreen, resizable, model, dependent, close, alwaysRaised,fullscreen',
					{url: url });
};

/**
 * 
 */
Zool.confirm = function(title, content) {	
	return Sys.services.prompts().confirm(window, title, content);	
};

Zool.openWindow = function(url, mode, width, height) {
	
	mode = mode || 'chrome, centerscreen, resizable, model, dependent, close, alwaysRaised,fullscreen';
	
	if(width){
		mode += ', width='+ width;
	}
	if(height){
		mode += ', height='+ height;
	}
	
	window
	.openDialog(
			'chrome://myapp/content/base.xul',
			'showmore',
			mode ,
			{url: url,
			mainWindow: Zool.mainWindow});
};

Zool.popup = function(title, text) {
	try {		
		Components.classes['@mozilla.org/alerts-service;1'].getService(
				Components.interfaces.nsIAlertsService).showAlertNotification(
				null, title, text, false, '', null);
	} catch (e) {
		// prevents runtime error on platforms that don't implement
		// nsIAlertsService
	}
};

/**
 * @var url url of file
 */
// TODO nem működik
Zool.print = function(url){
	
	var win = document.getElementsByTagName('window')[0];
	
	var iframe = document.createElementNS(NAMESPACES.HTML,'iframe');
	
	iframe.setAttribute('style', 'display: none;');
	iframe.setAttribute('src', url);
	
	iframe.onload = function (){
		iframe.print();
	};
	
	win.appendChild(iframe);
	
};

Zool.renderedScript = '';

Zool.namespaces = {};

Zool.defaultNamespaceUri = null;

Zool.registerNamespaces = function(attributes){
	
	// find new namespaces
	for(attributeName in attributes){
		
		// this is an namespace definition
		if(attributeName.indexOf('xmlns:') == 0){
			
			var namespace = attributeName.substring(6);
			var namespaceUri = attributes[attributeName];
			
			Zool.namespaces[namespace] = namespaceUri;
			
		}else if( attributeName == 'xmlns'){
			
			Zool.defaultNamespaceUri = attributes[attributeName];		
			
		}
		
	}
	
};

Zool.buildElementByJson = function(elem, isScript) {
	
	if (typeof elem == 'string' && !isScript) {		
		return document.createCDATASection(elem);
	}else if(isScript){
		eval(elem);		
		return document.createCDATASection('');
	}
	
	var name = elem[0];
	var attributes = elem[1] || {};
	var children = elem[2] || [];
	
	var element;
	
	Zool.registerNamespaces(attributes);
	
	
	if(name.indexOf(':') > -1){
		
		// todo
		var nameSpaceAndName = name.split(':');
		name = nameSpaceAndName[1];
		var namespaceURI = Zool.namespaces[nameSpaceAndName[0]];
		
		element = document.createElementNS(namespaceURI, name);
		
	}else{
		
		if(Zool.defaultNamespaceUri == null){
			element = document.createElement(name);
		}else{
			
			element = document.createElementNS(Zool.defaultNamespaceUri, name);
			
		}
		
	}
	
	
	for (key in attributes) {
		var value = attributes[key];
		element.setAttribute(key, value);
	}
	for (i in children) {
		var child = Zool.buildElementByJson(children[i], name == 'script');
		element.appendChild(child);
	}

	return element;

};

Zool.reRenderElement = function(id, content) {

	if (!content)
		return;
	
	var element = document.getElementById(id);

	if (!element){
		Sys.log('No element with id: '+ id);
		return;
	}

	/*
	 * Special handling
	 */
	if (element.nodeName == 'window' || element.nodeName == 'dialog') {
		
		Zool.registerNamespaces(content[1]);
		
		for (attri in content[1]) {
			var attr = content[1][attri];
			try {
				document[attri] = attr;
			} catch (e) {
				Sys.log(e);
			}			
		}
		
		Zool.removeChildren(element);

		// children
		for (chi in content[2]) {
			var child = content[2][chi];
			child = Zool.buildElementByJson(child);

			element.appendChild(child);
		}

		return;
	}

	var parent = element.parentNode;

	var newElement = Zool.buildElementByJson(content);

	parent.insertBefore(newElement, element);
	parent.removeChild(element);

};

/**
 * Remove all the children from element.
 */
Zool.removeChildren = function(element){
	while (element.hasChildNodes()) {
		element.removeChild(element.childNodes[0]);
	}
};

/**
 * Finds a subtree by id
 */
Zool.getContentPart = function(tree, id){
	
	if(tree[1] && tree[1]['id'] && tree[1]['id'] == id){
		// this is it
		return tree;
	}else{
		
		for(i in tree[2]){			
			var child = tree[2][i];			
			var found =  Zool.getContentPart(child, id);
			
			if(null !== found){
				return found;
			}			
			
		}
		
	}
	
	return null;
	
};

Zool.reRenderByJson = function(json, config) {
	if(!json) return;
	
	var head = json[0];
	var window = json[1];
	
	if(config){
			
		if(config['reRender']){
			
			var reRender = config['reRender'];
			
			if(reRender.indexOf(',') != -1){
				reRender = reRender.split(',').map(function(i){ return i.trim(); });				
			}else{
				reRender = [reRender.trim()];
			}
					
			for(index in reRender){
				var id = reRender[index];
				var content = Zool.getContentPart(window, id);
				
				Zool.reRenderElement(id, content);
				
			}
			
			return;
		}
		
	}
	
	/*
	 * Special handling
	 */
	if (window[0] == 'window' || window[0] == 'dialog') {
		
		var element = document.getElementById('main'); 
		
		Zool.registerNamespaces(window[1]);
		
		for (attri in window[1]) {
			var attr = window[1][attri];
			try {
				document[attri] = attr;
				element.setAttribute(attri, attr);
				
//				var setter = 'set' + attri[0].toUpperCase() + attri.slice(1);
//				d(setter);
//				eval('element.' + setter + '(' +attr+ ');');
				
			} catch (e) {
				try{
					element.setAttribute(attri, attr);
				}catch(e){
					d(e);
				}
			}			
		}
		
		// remove all the childs
		while (element.hasChildNodes()) {
			element.removeChild(element.childNodes[0]);
		}

		// build and append children
		for (chi in window[2]) {
			var child = window[2][chi];
			child = Zool.buildElementByJson(child);

			element.appendChild(child);
		}

		return;
	}
	
	//Zool.reRenderElement(id, json[id]);
	
};

// Extendable handlers.
Zool.prefixHandlers = {};

Zool.handleResponse = function(json, config) {

	
	if(!json)return;

	
	if (json.error) {
		Zool.popup('Error', json.error);
		//return;
	}

	try{
		d(config);
	}catch(e){}
	
	var reRender = true;
	if(config && typeof config.reRender != 'undefined' && ( config.reRender == 'false' || config.reRender === false)){
		reRender = false;
	}
		
	// reRender if required
	if(reRender){
		if (json.content) {		
			Zool.reRenderByJson(json.content, config);
		}
	}
	
	var onloadScripts = new Array();
	
	// process script
	if(json.prefix){		
		
		if(json.prefix.scripts){
			var scripts = json.prefix.scripts;
			
			for(key in scripts){
				
				// TOOD precedencia
				var script = scripts[key];
				
				if(key == 'POS_LOAD'){
					onloadScripts.push(script);
				}else{
					try{
						eval(script);
					}catch(e){
						Sys.log(e);
					}
				}
			}			
		}
		
		if(json.prefix.cssFiles){
			var cssFiles = json.prefix.cssFiles;
			
			for(i in cssFiles){
				var url = cssFiles[i];
				
				try{

					d("Load stylesheet: " + url);
					
					var sss = Components.classes["@mozilla.org/content/style-sheet-service;1"]
							.getService(Components.interfaces.nsIStyleSheetService);
					var ios = Components.classes["@mozilla.org/network/io-service;1"]
							.getService(Components.interfaces.nsIIOService);
					
					
					var uri = ios.newURI(url, null, null);
					sss.loadAndRegisterSheet(uri, sss.USER_SHEET);
					
				}catch(e){
					d('Error while loading CSS file: ' + e);
				}
				
			}
			
		}
		
		for(key in json.prefix){
			
			// handle prefix with own handler
			if(Zool.prefixHandlers[key] && typeof Zool.prefixHandlers[key] == 'function'){
				Zool.prefixHandlers[key](json.prefix[key]);
			}
			
		}
		
		
	}// end of prefix process

	if (json.messages) {
		for(i in json.messages)
			Zool.popup(i, json.messages[i]);
	}
	
	// run onload scripts
	onloadScripts.forEach(function(script){
		try{
			eval(script);
		}catch(e){
			Sys.log(e);
		}
	});
	
	// config dependent code
	if(config){
	
		// callback
		if(config.oncomplete){
			if(typeof config.oncomplete == 'function'){
				config.oncomplete(json);
			}else{
				
				try{
					// rename json
					var data = json;
					eval(config.oncomplete);
				}catch(e){
					Sys.log(e);
				}				
			}
		}
		
	}	
	

};
// // end of Zool