

//function getWebBrowserPrint()
//{
//return _content.QueryInterface(Components.interfaces.nsIInterfaceRequestor)
//.getInterface(Components.interfaces.nsIWebBrowserPrint);
//}



/**
function loadApplication() {

    Sys.debug('Init Yii app');
    DEBUG = true;
    
    //Sys.file.getFile('c:\\windows\\system32\\calc.exe').launch();
    
    Sys.log(Sys.services.runtime());

    var url = "http://localhost/szakdoga/index.php?r=";
    
    var config = {
      initUrl : url  
    };
    
   // var yii = new Yii.Connection(config);
    
    //yii.run(window);
    
    let prompts =
  Cc["@mozilla.org/embedcomp/prompt-service;1"].
    getService(Ci.nsIPromptService);
}
*/

// debugger
function d(d){
	Sys.dump(Sys.log(d, true));
}

// select mainwindow
if(window.arguments[0] && window.arguments[0].mainWindow){
	Zool.mainWindow = window.arguments[0].mainWindow;
}

var App = {};

/**
 * Handles tags.
 */
App.inputHandlers = {
		
		textbox: function(elem){
			
			// file upload check
			var uploadClass = 'file-upload';
			if(elem.getAttribute('class') && elem.getAttribute('class').indexOf(uploadClass) > -1){
				return elem.thefile;				
			}
			
			return elem.value;
		},
		
		checkbox: function(elem){
			return elem.checked;
		},
		
		menulist: function(elem){
			return elem.selectedItem.value;
		},
		
		radiogroup: function(elem){			
			return elem.selectedItem.value;			
		},
		
		listbox: function(elem) {
			var ret = [];
			for(var i=0; i < elem.selectedItems.length; i++)
				ret.push(elem.selectedItems[i].value);
			
			return ret.join();
		},
		
		richlistbox: function(elem){
			
			if(elem.getAttribute('class') && elem.getAttribute('class').indexOf('checkboxed') > -1){
				
				var ret = [];
		
				var children = elem.childNodes;		
				for(var i=0; i<children.length; i++){
					var item = children[i];					
					var itemChildren = item.childNodes;
					
					for(var j=0; j<itemChildren.length; j++){
						var itemChild = itemChildren[j];
						var name = itemChild.tagName.toLowerCase();
						// search a checkbox
						if(name == 'checkbox' && itemChild.checked)							
							ret.push(item.value);
					}
				}
			
				return ret.join();
				
			}
			
			var ret = [];
			for(var i=0; i < elem.selectedItems.length; i++)
				ret.push(elem.selectedItems[i].value);
			
			return ret.join();
			
		}
		// TODO			
};

/**
 * Returns the 
 */
App.inputValueGetter = function(elem){
	
	var tagName = elem.tagName.toLowerCase();
	
	d('Get value of: ' + tagName);
	
	if(tagName in App.inputHandlers){		
		return App.inputHandlers[tagName](elem);		
	}
	
	return elem.value; // try something
	
	
};

App.beforeAjax = function(){};
App.afterAjax = function(){};

App.link = function(linkUrl, config){
	
	App.beforeAjax(linkUrl, config);
	
	Sys.ajax({
		url: linkUrl,
		parse: 'json',
		data: config ? (config.data) : null,				
		method: config ? (config.method ? config.method : 'GET') : 'GET',
		success: function(data){
			Zool.handleResponse(data, config);
			
			App.afterAjax(data);
			
		},
		error: function(e){
			Zool.popup('Error', 'Application is unavailable.');
			Sys.log(e);
			
			App.afterAjax(null);
		}
	});	
};

App.submit = function(linkUrl, config){
	
	// callback
	if(config.onbeforesubmit){
		
		d(config.onbeforesubmit);
		
		if(typeof config.onbeforesubmit == 'function'){
			config.onbeforesubmit(json);
		}else{
			
			try{
				eval(config.onbeforesubmit);
			}catch(e){
				Sys.log(e);
			}
			
		}
	}
	
	// get input IDs
	inputIds = config.inputIdList;
	try{
		inputIds = eval(inputIds);
	}catch(e){ 
		// cannot evaluate
		inputIds = config.inputIdList;
	}
	
	
	
	// assemble data
	//var formData = {};
	var formData = new FormData();
	
	if(inputIds != ""){
		
		var inputIdKeys = inputIds.split(config.inputIdSeparator);
		for(index in inputIdKeys){
			
			// input ID and variable mapping
			// inputId=Model[attribute]
			var pair = inputIdKeys[index].split('=');
			
			var id = pair[0];
			var varName = pair[1];
	
			var elem = Zool.byId(id);		
			var value = App.inputValueGetter(elem);
			
			//formData[varName] = value;
			formData.append(varName, value);
			
		}
	}
	
	App.beforeAjax(linkUrl, config);
	
	// submit
	Sys.ajax({
		url: linkUrl,
		parse: 'json',
		data: formData,
		filebox: config.filebox, // just for test
		method: config ? (config.method ? config.method : 'GET') : 'GET',
		success: function(data){
			Zool.handleResponse(data, config);
			
			App.afterAjax(data);
		},
		error: function(e, data){
			d('Error at submit form:' +  e);
			d('Data of error: ' + data);
			
			Zool.popup('Server fail', data);
			
			App.afterAjax(data);
		}
	});	
	
	
	
};


function loadApplication() {

	Sys.DEBUG = true;
	
	Zool.mainWindow = window;
	
	App.link("http://127.0.0.1/szakdoga?r=site/login");

}


