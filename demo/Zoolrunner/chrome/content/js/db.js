var dserv = Components.classes["@mozilla.org/file/directory_service;1"]
		.getService(Components.interfaces.nsIProperties);

// Here’s where the file will end up in the profile directory
var file = dserv.get("ProfD", Components.interfaces.nsIFile);
file.append("workship.db");

// This is the template file
var ours = dserv.get("AChrom", Components.interfaces.nsIFile);

ours.append("workship.db");

// Copy if we haven’t got it
if (!file.exists())
	ours.copyTo(dserv.get("ProfD", Components.interfaces.nsIFile),
			"workship.db");

var storageService = Components.classes["@mozilla.org/storage/service;1"]
		.getService(Components.interfaces.mozIStorageService);

var mDBConn = storageService.openDatabase(file);


function doSQL(sql, targetClass, callback) {
    var rv = new Array;
    var statement = mDBConn.createStatement(sql);
    while (statement.executeStep()) {
        var c;
        var thisArray = new Array;
        for (c=0; c < statement.numEntries; c++) {
            thisArray.push(statement.getString(c));
        }
        var thing = new targetClass(thisArray);
        if (callback) { callback(thing); }
        rv.push(thing);
    }
    
    return rv;
}

function Song (myA) { 
    this.id = myA[0]; 
    this.title = myA[1];
}

Song.prototype.toString = function() {
	return 'Song[id='+this.id+', title='+this.title+']';
};

Song.retrieve = function (id) {
    var arr = doSQL("SELECT * FROM song WHERE id = "+id, Song);
    if (arr) { return arr[0]; }
};

Song.retrieveAll = function (callback) {
    return doSQL("SELECT * FROM song", Song, callback);
};


function auto_increment_value () {
    var statement = mDBConn.createStatement("SELECT last_insert_rowid()");
    statement.executeStep();
    return statement.getInt32(0);
}
Song.create = function (name) {
    var statement = mDBConn.createStatement(
        "INSERT INTO song (title) VALUES (?1)"
    );
    statement.bindStringParameter(0, name);
    statement.execute();
    return new Song([auto_increment_value(), name]);
};

// create table
(function(){
	/*
	var statement = mDBConn.createStatement(
        "CREATE TABLE IF NOT EXISTS song (id INTEGER PRIMARY KEY, title TEXT);"
    );    
    statement.execute();
    
    /*for(var i=0; i < 10; i++)
    	Song.create('Song' + i);
    */    
//    Song.retrieveAll(function(song){
//    	
//    	d(song.toString());
//    });
//    
})();
